import { Rectangle, ScreenPos, TilePos, roundTilePos } from "./geo/geo"
import { Graphics, TILES_ON_SCREEN } from "./graphics/graphics"
import { Averager } from "./structures/util/averager"
import { World } from "./world/world"

const world = new World()
const canvas: HTMLCanvasElement = document.getElementById('mainCanvas') as HTMLCanvasElement

const text: HTMLElement = document.getElementById('text')!
text.hidden = true


let lastRender = 0

let graphics = new Graphics(canvas, world)
const ctx = graphics.ctx

world.menu = graphics.menu // PFUSCH!!


const inputHandler = graphics.inputHandler

let stepTime = new Averager(50, 0)


window.setInterval(() => {
    inputHandler.handleInput()
    const t1 = Date.now()
    const delta = Date.now() - (lastRender ?? Date.now()) 
    if(!graphics.paused) world.step(delta)
    stepTime.record(Date.now() - t1)
    inputHandler.handleWasd(world)
    lastRender = Date.now()
    render(world)
}, 1000 / Graphics.FPS )

function render(world: World): void{
    ctx.resetTransform()
    ctx.clearRect(0, 0, 3000, 2000)
    
    //ctx.fillText(Math.floor(1000.0 / elapsed) + "FPS", 16*Graphics.TILE_SIZE, 7 * Graphics.TILE_SIZE)
    //ctx.fillText(stepTime.val + " time", 16*Graphics.TILE_SIZE, 7 * Graphics.TILE_SIZE)
    
    // render Minimap
    let roundedAnc: TilePos = roundTilePos(graphics.ancPos)
    let firstRenderPos = graphics.tilePosToScreenPos(roundedAnc)
    ctx.save()
    ctx.beginPath()
    ctx.rect(5, 5, TILES_ON_SCREEN * Graphics.TILE_SIZE, TILES_ON_SCREEN * Graphics.TILE_SIZE)
    ctx.stroke()
        
    ctx.clip()
    ctx.translate(firstRenderPos.x, firstRenderPos.y)
    world.render(graphics, roundTilePos(graphics.ancPos), {w: TILES_ON_SCREEN + 1, h: TILES_ON_SCREEN + 1})
    ctx.restore()
    graphics.menu.render(graphics)

    ctx.save()
    world.renderResourceBar(ctx, {x: Graphics.tsize(15.1), y: Graphics.tsize(0.5)})
    
    ctx.restore()


    ctx.save()
    ctx.beginPath()
    ctx.rect(5, 5, TILES_ON_SCREEN * Graphics.TILE_SIZE, TILES_ON_SCREEN * Graphics.TILE_SIZE)
    ctx.clip()
    // render frame around locked tile
    if(graphics.lockedTile){
        ctx.save()
        ctx.beginPath()
        let pos = graphics.tilePosToScreenPos(graphics.lockedTile.pos)
        ctx.rect(pos.x, pos.y, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        ctx.lineWidth = 4
        ctx.strokeStyle = "yellow"
        ctx.stroke()
        ctx.restore()
    } 
    ctx.restore()

    ctx.beginPath()
    // Render StructInfo
    const infoBoxHeight = Graphics.tsize(10)
    const infoBoxY0 = Graphics.tsize(1)
    ctx.save()
    ctx.translate(Graphics.tsize(4), 0)
    let tile = world.tileAtPos(inputHandler.mouseTilePos)
    if(graphics.lockedTile) tile = graphics.lockedTile
    if(tile){
        ctx.translate(Graphics.tsize(16.1), infoBoxY0)
        ctx.strokeRect(Graphics.tsize(-0.1), -Graphics.tsize(0.6), Graphics.tsize(6.5), infoBoxHeight)
        ctx.translate(Graphics.tsize(0.2), 0)
        tile.structure.renderStructInfo(graphics)
    } else{
    }
    ctx.restore()

    ctx.save()
    //const minimapPos = {x: Graphics.tsize(21), y: infoBoxHeight + infoBoxY0 }
    const minimapPos = {x: Graphics.tsize(17), y: Graphics.tsize(1) }

    const minimapTileSize = 4
    ctx.translate(minimapPos.x, minimapPos.y)
    graphics.renderMinimap(graphics, world, minimapTileSize)

    inputHandler.onClickInRect(new Rectangle(minimapPos.x, minimapPos.y, world.w * minimapTileSize, world.h * minimapTileSize), (mousePos?: ScreenPos) => {
        if(mousePos){
            graphics.ancPos = {i: Math.max(0, mousePos.y / minimapTileSize - 8), j: Math.max(0, mousePos.x / minimapTileSize -8)}
        }
        
    }, true)
    ctx.restore()


        
}