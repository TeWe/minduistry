import { loadImage } from "../util"
import { Resource } from "./resource"
import { ResourceType } from "./resTypes"

export class Copper extends Resource{
    
    get renderImage(){
        return image
    }
    
    get resType(): ResourceType{
        return ResourceType.COPPER
    }
}

const image = loadImage("resources/copper.png")