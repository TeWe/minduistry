import { loadImage } from "../util"
import { Resource, } from "./resource"
import { ResourceType } from "./resTypes"

export class Coal extends Resource{
    
    get renderImage(){
        return Coal.image
    }

    get resType(): ResourceType{
        return ResourceType.COAL
    }

    static get image(): HTMLImageElement{
        return image
    }
}

const image: HTMLImageElement = loadImage("resources/coal.png")