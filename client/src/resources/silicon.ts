import { loadImage } from "../util"
import { ResourceType } from "./resTypes"
import { Resource } from "./resource"

export class Silicon extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.SILICON
    }
}

const image = loadImage("resources/silicon.png")