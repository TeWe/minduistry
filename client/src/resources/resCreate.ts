import { Coal } from "./coal"
import { Cocaine } from "./cocaine"
import { Coin } from "./coin"
import { Copper } from "./copper"
import { Morkite } from "./morkite"
import { ResourceType } from "./resTypes"
import { Resource } from "./resource"
import { Salad } from "./salad"
import { Sand } from "./sand"
import { Silicon } from "./silicon"

export function createResource(type: ResourceType): Resource{
    switch(type){
        case ResourceType.COAL: return new Coal()
        case ResourceType.COPPER: return new Copper()
        case ResourceType.SALAD: return new Salad()
        case ResourceType.SAND: return new Sand()
        case ResourceType.SILICON: return new Silicon()
        case ResourceType.COIN: return new Coin()
        case ResourceType.COCAINE: return new Cocaine()
        case ResourceType.MORKITE: return new Morkite()
    }
}
