import { ResourceType } from "./resTypes"
import { ResourceSet } from "./resourceSet"

export class ResourceStorage extends ResourceSet{
    totalCap: number
    perResourceCap: number
    total: number

    constructor(totalCap: number, perResourceCap: number){
        super()
        this.totalCap = totalCap
        this.perResourceCap = perResourceCap
        this.total = 0
    }   

    hasSpaceFor(type: ResourceType){
        if(this.total >= this.totalCap) return false
        if(this.get(type) >= this.perResourceCap) return false
        return true
    }


}