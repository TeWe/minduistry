import { loadImage } from "../util"
import { Resource } from "./resource"
import { ResourceType } from "./resTypes"

export class Coin extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.COIN
    }
}

const image = loadImage("resources/coin.png")