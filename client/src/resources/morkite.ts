import { loadImage } from "../util"
import { ResourceType } from "./resTypes"
import { Resource } from "./resource"

export class Morkite extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.MORKITE
    }
}

const image = loadImage("resources/morkite.png")