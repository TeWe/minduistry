import { loadImage } from "../util"
import { ResourceType } from "./resTypes"
import { Resource } from "./resource"

export class Sand extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.SAND
    }
}

const image = loadImage("resources/sand.png")