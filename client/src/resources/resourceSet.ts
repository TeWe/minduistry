import { ResourceType } from "./resTypes"
import { Resource } from "./resource"


export class ResourceSet{

    private data: Partial<Record<ResourceType, number>>

    constructor(...ar: [ResourceType, number][]){
        this.data = {}

        for(let [type, amount] of ar){
            this.add(type, amount)
        }
    }

    isEmpty(): boolean{
        let empty = true
        this.forEach((_, amount) => {
            if(amount > 0) empty = false
        })
        return empty
    }

    add(type: ResourceType, amount: number = 1): void{
        if(!this.data[type]) this.data[type] = amount
        else this.data[type]! += amount
    }

    remove(res: ResourceType, amount: number = 1){
        if(!this.data[res]) return
        this.data[res] = Math.max(0, this.data[res]! - amount)
    }

    get(type: ResourceType): number{
        return this.data[type] ?? 0
    }

    forEach(f: (type: ResourceType, amount: number) => void){
        Resource.forEachType(type => {
            f(type, this.get(type))
        })
    }

    hasAtLeast(type: ResourceType, am: number = 1){
        return this.get(type) >= am
    }
}