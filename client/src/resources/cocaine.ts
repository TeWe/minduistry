import { loadImage } from "../util"
import { Resource } from "./resource"
import { ResourceType } from "./resTypes"

export class Cocaine extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.COCAINE
    }
}

const image = loadImage("resources/kokain.png")