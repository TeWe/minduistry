export enum ResourceType{
    COPPER = "COPPER",
    COAL = "COAL",
    SALAD = "SALAD",
    SAND = "SAND",
    SILICON = "SILICON",
    COIN = "COIN",
    COCAINE = "COCAINE",
    MORKITE = "MORKITE"
}


export const resourceTypes: ResourceType[] = Object.values(ResourceType)
