import { loadImage } from "../util"
import { ResourceType } from "./resTypes"
import { Resource } from "./resource"

export class Salad extends Resource{
    
    get renderImage(){
        return image
    }

    get resType(): ResourceType{
        return ResourceType.SALAD
    }
}

const image = loadImage("resources/salad.png")