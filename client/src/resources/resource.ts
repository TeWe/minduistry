import { ScreenPos } from "../geo/geo"
import { Graphics } from "../graphics/graphics"
import { Transform } from "../structures/structure"

import { ResourceType, resourceTypes } from "./resTypes"

export abstract class Resource{

    private rotation = 0

    constructor(){
        this.rotation = 0
    }

    get name(){
        return "unbekannt"
    }

    rotateBy(delta: number){
        this.rotation += delta
        if(this.rotation < 0) this.rotation += 2*Math.PI
        if(this.rotation > 2 * Math.PI) this.rotation -= 2*Math.PI
    }

    render(ctx: CanvasRenderingContext2D, pos: ScreenPos, parentTransform: Transform = {rotValue: 0, mirrorValue: false}, size: number = Graphics.RES_SIZE){
        ctx.save()

        if(parentTransform.mirrorValue){
            ctx.translate(Graphics.TILE_SIZE, 0)
            ctx.scale(-1, 1)
            pos = {x: Graphics.TILE_SIZE - pos.x, y: pos.y}
            
        }

        ctx.translate(pos.x, pos.y)
        ctx.translate(-size / 2, - size / 2)
        

        const r = size * 1 /  Math.sqrt(2)

        let effectiveRotation = this.rotation - parentTransform.rotValue * 0.5 * Math.PI
        if(effectiveRotation < 0) effectiveRotation += 2*Math.PI

        ctx.translate(- Math.cos(effectiveRotation + 0.25 * Math.PI) * r, - Math.sin(effectiveRotation + 0.25 * Math.PI) * r)
        ctx.translate(+ Math.cos(0.25 * Math.PI) * r, + Math.sin(0.25 * Math.PI) * r)
        
        ctx.rotate(effectiveRotation)

        ctx.drawImage(this.renderImage, 0, 0, size, size)
        
        ctx.restore()
    }

    abstract get renderImage(): HTMLImageElement

    abstract get resType(): ResourceType

    static forEachType(f: (type: ResourceType) => void): void{
        resourceTypes.forEach(f)
    }

    
}
