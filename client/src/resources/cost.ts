import { ResourceType } from "./resTypes"
import { Resource } from "./resource"
import { ResourceSet } from "./resourceSet"

export class Cost extends ResourceSet{ 
    constructor(...ar: [ResourceType, number][]){
        super(...ar)
    }

    pay(resType: ResourceType, am: number = 1){
        this.remove(resType, am)
    }

    isPaid(): boolean{
        return this.isEmpty()
    }

    accepts(resource: Resource): boolean{
        let found = false
        this.forEach((t, am) => {
            if(am > 0 && t == resource.resType) return found = true
        })
        return found
    }
}