import { Rectangle, ScreenPos, TilePos, roundTilePos, screenPos, tilePos, tilePosEq } from "../geo/geo"
import { Tile } from "../world/tile"
import { World } from "../world/world"
import { Graphics } from "./graphics"

export class InputHandler{
    private leftClick: boolean = false
    private rightClick: boolean = false
    private keyState: Record<"KeyW" |  "KeyA" | "KeyS" | "KeyD", boolean>

    mousePos: ScreenPos = {x: 0, y: 0}

    mouseTilePos: TilePos = {j: 0, i: 0}

    graphics: Graphics

    private onRightClick: () => void = () => {}
    private onLeftClick: (pos?: ScreenPos) => void = () => {}

    constructor(graphics: Graphics) {

        this.graphics = graphics

        this.keyState = {
            "KeyW": false,
            "KeyA": false,
            "KeyS": false,
            "KeyD": false,
        }

        this.mouseTilePos = { j: 0, i: 0 }

        const canvas = graphics.canvas

        canvas.addEventListener('mousemove', event => {
            this.mousePos = screenPos(event.clientX - graphics.margins.left, event.clientY - graphics.margins.top)
            this.mouseTilePos = graphics.renderPosToTilePos(this.mousePos)
        })

        canvas.addEventListener('contextmenu', event => {
            event.preventDefault()
            this.rightClick = true
        })

        document.addEventListener('keydown', event => {
            switch(event.code){
                case "KeyW": 
                case "KeyS": 
                case "KeyD":
                case "KeyA":
                    this.keyState[event.code] = true
                    break
                case "KeyQ": 
                    graphics.menu.resetRenderStr()
                    break
                case "KeyR":
                    if(graphics.menu.renderStr){
                        graphics.menu.renderStr.rotate(1)
                    }
                    break
                case "KeyC":
                    graphics.showPower = !graphics.showPower
                    break
            }            
        })

        document.addEventListener('keyup', event => {
            switch(event.code){
                case "KeyW": 
                case "KeyS": 
                case "KeyD":
                case "KeyA":
                    this.keyState[event.code] = false
            }            
        })

        document.addEventListener('keypress', event => {
            event.stopPropagation();

            switch(event.code){
                case "Space": graphics.paused = !graphics.paused
            }            
        })


        

        canvas.addEventListener('click', e => {
            e.stopPropagation();
            e.preventDefault();
            if (e.which == 1){
                this.leftClick = true;

            }
            if (e.which == 3)
                this.rightClick = true;        
                
        })

        canvas.addEventListener('wheel', (event) => {
            if(graphics.menu.renderStr){
                graphics.menu.rotateRenderStr(event.deltaY)
            }
        });

    }


    onRightClickInTile(tile: Tile, f: () => void){
        if(this.mouseInTile(tile)) {
            this.onRightClick = f   
        }
    }


    onClickInTile(tile: Tile, f: () => void){
        if(this.mouseInTile(tile)) {
            this.onLeftClick = f   
        }
    }

    onClickInRect(rect: Rectangle, f: (pos?: ScreenPos) => void, offMainScreen: boolean = false): void{
        if(!offMainScreen && !this.mouseInBoundingRect()) return
        if(this.mouseInRect(rect, offMainScreen)) {
            this.onLeftClick = (mousePos?: ScreenPos) => mousePos && f({x: mousePos.x - rect.x0, y: mousePos.y - rect.y0})
        }
    }

    mouseInRect(rect: Rectangle, offMainScreen: boolean = false){
        if(!offMainScreen && !this.mouseInBoundingRect()) return false
        return rect.contains(this.mousePos)
    }

    mouseInTile(tile: Tile){     
        if(!this.mouseInBoundingRect()) return false
        return tilePosEq(this.mouseTilePos, tile.pos)
    }
    

    mouseInBoundingRect(): boolean{
        return this.graphics.boundingRect.contains(this.mousePos)
    }

    handleWasd(world: World){
        const speed = 0.3
        if(this.keyState.KeyW)
            this.graphics.ancPos = tilePos(this.graphics.ancPos.j, Math.max(0, this.graphics.ancPos.i - speed))
        if(this.keyState.KeyS)
            this.graphics.ancPos = tilePos(this.graphics.ancPos.j, Math.min(world.w - Graphics.MAP_HEIGHT - 0.01, this.graphics.ancPos.i + speed))
        if(this.keyState.KeyD)
            this.graphics.ancPos = tilePos(Math.min(world.w - Graphics.MAP_WIDTH - 0.01, this.graphics.ancPos.j + speed), this.graphics.ancPos.i)
        if(this.keyState.KeyA)
            this.graphics.ancPos = tilePos(Math.max(0, this.graphics.ancPos.j - speed), this.graphics.ancPos.i)         
    }

    handleInput(){
        if(this.leftClick && this.onLeftClick)
            this.onLeftClick(this.mousePos)
        if(this.rightClick){
            if(this.graphics.menu.renderStr) this.graphics.menu.deselectRenderStr()
            else if(this.onRightClick) this.onRightClick()
            
        }
        this.onLeftClick = () => {}
        this.onRightClick = () => {}
        this.leftClick = false
        this.rightClick = false

    }

}