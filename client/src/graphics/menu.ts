import { Rectangle, ScreenPos, TilePos } from "../geo/geo"
import { Graphics } from "./graphics"
import { MinduistryRenderingContext } from "../graphics/renderingContext"
import { Empty, Structure } from "../structures/structure"
import { World } from "../world/world"
import { Pipe } from "../structures/pipes/pipe"
import { CentralStorage } from "../structures/central_storage"
import { Splitter } from "../structures/pipes/splitter"
import { Joiner } from "../structures/pipes/joiner"
import { Junction } from "../structures/pipes/junction"
import { Laboratory } from "../structures/laboratory"
import { Furnace } from "../structures/furnace"
import { Pickaxe } from "../structures/mines/pickaxe"
import { Shovel } from "../structures/mines/shovel"
import { Garden } from "../structures/mines/garden"
import { IronPickaxe } from "../structures/mines/iron_pickaxe"
import { SiliconPress } from "../structures/factories/silicon_press"
import { CoinSmelter } from "../structures/factories/coin_smelter"
import { Dealer } from "../structures/factories/dealer"
import { PowerNode } from "../structures/power/power_node"
import { SolarPanel } from "../structures/power/solar_panel"
import { Gruft } from "../structures/wonders/gruft"


const enum BarType{
    PIPE = "PIPE",
    MINE =  "MINE",
    ETC = "ETC",
    FABRIC = "FABRIC",
    POWER = "POWER",
    WONDER = "WONDER",
    OVEN = "OVEN"
}


export class Menu{

    lockedBar?: BarType
    lockedMenuItem?: MenuItem
    menuItems: Record<BarType, MenuItem[]>
    rotationState: 0 | 1 | 2 | 3
    renderStr?: Structure 

    world: World

    constructor(world: World){
        this.lockedBar = BarType.PIPE

        this.world = world
        world.menu = this

        this.rotationState = 0

        this.menuItems = this.createItemRecord()
    }

    get menuPos(): ScreenPos{
        return {x: Graphics.tsize(15.5), y: Graphics.tsize(4.6)}

    }

    render(graphics: Graphics){
        graphics.ctx.save()
        const y0 = this.menuPos.y
        const x0 = this.menuPos.x
        graphics.ctx.strokeRect(x0 - Graphics.tsize(0.1), y0  - Graphics.tsize(0.4) , Graphics.tsize(4.5), Graphics.tsize(10.8))

        this.renderMenuBar(graphics, BarType.PIPE, "Pipes", x0, y0,  Graphics.tsize(1.5))
        this.renderMenuBar(graphics, BarType.MINE, "Minen", x0 + Graphics.tsize(2.3) , y0 , Graphics.tsize(1.6))
        this.renderMenuBar(graphics, BarType.FABRIC, "Fabriken", x0, y0+ Graphics.tsize(1), Graphics.tsize(2.1))
        this.renderMenuBar(graphics, BarType.POWER, "Strom", x0 + Graphics.tsize(2.3) , y0+ Graphics.tsize(1), Graphics.tsize(1.5))
        this.renderMenuBar(graphics, BarType.WONDER, "Wunder", x0 , y0+ Graphics.tsize(2), Graphics.tsize(2))
        this.renderMenuBar(graphics, BarType.ETC, "ETC", x0+ Graphics.tsize(2.3) , y0 + Graphics.tsize(2), Graphics.tsize(1.5))
        this.renderSelectedBar(graphics)
        graphics.ctx.restore()
    }

    renderSelectedBar(graphics: Graphics){
        let items = this.menuItems[this.lockedBar!]
        if(!items) return 
        items.forEach(item => item.render(graphics))
    }

    renderMenuBar(graphics: Graphics, type: BarType, text: string, x0: number, y0: number, width: number){
        const {ctx, inputHandler, context} = graphics
        ctx.save()
    
        ctx.font = (type == this.lockedBar ? "bold " : "") + Graphics.TILE_SIZE / 2 + "px Arial"
        
        ctx.textBaseline = "top";

        ctx.beginPath()
        ctx.fillText(text, x0 + Graphics.tsize(0.1), y0)

        let rect = new Rectangle(x0, y0 - 0.3 * Graphics.TILE_SIZE , width, Graphics.TILE_SIZE)
        if(inputHandler.mouseInRect(rect, true)){
            ctx.beginPath()
            ctx.rect(rect.x0, rect.y0, rect.w, rect.h)
            ctx.stroke()
        }

        inputHandler.onClickInRect(rect, () => {
            this.lockedBar = type
            this.lockedMenuItem = undefined
            this.deselectRenderStr()
        }, true)
    
        ctx.restore()
    }

    deselectRenderStr(){
        this.renderStr = undefined
        this.rotationState = 0
    }

    rotateRenderStr(deltaY: number){
        this.renderStr!.rotate(deltaY)
        if(deltaY > 0) this.rotationState = (this.rotationState + 4 + 1) % 4 as 0 | 1 | 2 | 3
        else this.rotationState = (this.rotationState + 4 - 1) % 4 as 0 | 1 | 2 | 3
    }
    
    createItemRecord(): Record<BarType, MenuItem[]>{
        let setPositions = (itemList: MenuItem[]) => {
            let x = 0
            let y = 0
            for(let item of itemList){
                item.setPos({x: this.menuPos.x + Graphics.tsize(x * 1.1), y: this.menuPos.y + Graphics.tsize(2.8 + 1.1 * y)})
                x++
                if(x >= 4){
                    x = 0
                    y++
                }
            }
            return itemList
        }   

        

        const pipeList = [
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.mirrorVertically()
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.mirrorVertically()
                t.rotate(1)
                return t
            }),
            
            
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.rotate(-1)
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.rotate(1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.mirrorVertically()
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("curved")
                t.mirrorVertically()
                t.rotate(-1)
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("straight")
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("straight")
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("straight")
                t.rotate(-1)
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                let t = new Pipe("straight")
                t.rotate(-1)
                t.rotate(-1)
                t.rotate(-1)
                return t
            }),
            new MenuItem(this, () => {
                return new Junction()
            }),

            new MenuItem(this, () => {
                return new Joiner()
            }),

            new MenuItem(this, () => {
                return new Splitter()
            }),
        ]

        const mineList = [
            new MenuItem(this, () => new Pickaxe()),
            new MenuItem(this, () => new Shovel()),
            new MenuItem(this, () => new Garden()),
            new MenuItem(this, () => new IronPickaxe()),

        ]

        const fabricList = [
            new MenuItem(this, () => new Furnace()),

            new MenuItem(this, () => new SiliconPress()),
            new MenuItem(this, () => new CoinSmelter()),
            new MenuItem(this, () => new Dealer()),



        ]

        const powerList = [
            new MenuItem(this, () => new PowerNode()),
            new MenuItem(this, () => new SolarPanel()),
            new MenuItem(this, () => new Furnace()),

        ]

        const wonderList = [
            new MenuItem(this, () => new Gruft()),
            //new MenuItem(this.ctx, () => new Furnace(), this.world),
        ]
        /*
        this.menuItems[Menu.bars.MILITARY] = [
            new MenuItem(this.ctx, () => new Bow(), this.world),
            new MenuItem(this.ctx, () => new Wall(), this.world),
        ]
        */

        const ovenList = [
            new MenuItem(this, () => new Furnace()),
            //new MenuItem(this.ctx, () => new Furnace(), this.world),
        ]

        const etcList = [
            new MenuItem(this, () => new Laboratory()),
            new MenuItem(this, () => new CentralStorage()),
        ]


        const menuItems = {
            [BarType.PIPE]: setPositions(pipeList),
            [BarType.MINE]: setPositions(mineList),
            [BarType.ETC]: setPositions(etcList),
            [BarType.WONDER]: setPositions(wonderList),
            [BarType.OVEN]: setPositions(ovenList),
            [BarType.FABRIC]: setPositions(fabricList),
            [BarType.POWER]: setPositions(powerList)
        }

        return menuItems
    }

    forAllMenuItems(f: (mi: MenuItem) => void): void{
        for(let cat of Object.values(this.menuItems)){
            for(let item of cat) f(item)
        }
    }

    resetRenderStr(){
        this.renderStr = undefined
        this.rotationState = 0
    }
}

class MenuItem{
    generator: () => Structure
    renderStr: Structure
    unlocked: boolean
    menu: Menu
    rect: Rectangle

    constructor(menu: Menu, generator: () => Structure){
        this.renderStr = generator()
        this.generator = generator
        this.menu = menu
        this.rect = new Rectangle(0, 0, 1, 1)
        
        let unlockAll = false
        if(!unlockAll){
            this.unlocked = false
                || this.renderStr instanceof Pipe 
                || this.renderStr instanceof Pickaxe 
                || this.renderStr instanceof Laboratory
        } else{
            this.unlocked = true 
        }
    }

    get world(): World{
        return this.menu.world
    }

    setPos(pos: ScreenPos): void{
        this.rect = new Rectangle(pos.x, pos.y, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    render(graphics: Graphics){
        const {ctx, inputHandler} = graphics
        if(!this.unlocked) return
        ctx.save()
        ctx.beginPath()
        ctx.translate(this.rect.x0, this.rect.y0)
        if(!this.world.isAffordable(this.renderStr.costs))
            ctx.globalAlpha = 0.6
        this.renderStr.renderBottomLayer(graphics)
        ctx.restore()

        if(this.menu.lockedMenuItem == this){
            ctx.save()
            ctx.beginPath()
            ctx.lineWidth = 5
            this.rect.ctxRect(ctx)
            ctx.stroke()
            ctx.restore()
        } 
        if(inputHandler.mouseInRect(this.rect, true)){
            ctx.save()
            this.rect.ctxRect(ctx)
            ctx.stroke()
            
            
            let x = this.menu.menuPos.x + Graphics.tsize(0.1)
            let y = this.menu.menuPos.y + Graphics.tsize(8.8)
            /*
            ctx.font = "bold " + 15 + "px Arial"
            ctx.fillText(this.renderStr.name, x, y - 20)
            */

            let cost = this.renderStr.costs

            Graphics.renderResourceSet(ctx, cost, {x, y}, 1.5 * Graphics.RES_SIZE, this.renderStr.name)

            Graphics.renderDescription(ctx, {x, y: y + Graphics.tsize(1.3)}, Graphics.tsize(0.5), this.renderStr.text)


            
            ctx.restore()
        }

        if(this.world.isAffordable(this.renderStr.costs)){
            inputHandler.onClickInRect(this.rect, () => {
                this.menu.lockedMenuItem = this
                this.menu.rotationState = 0
                this.menu.renderStr = this.generator()
                graphics.lockedTile = undefined
            }, true)  
        }
              
    }

    
}