import { Dimensions, Rectangle, ScreenPos, TilePos } from "../geo/geo"
import { MinduistryRenderingContext } from "./renderingContext"
import { Resource } from "../resources/resource"
import { Tile } from "../world/tile"
import { InputHandler } from "./inputHandler"
import { World } from "../world/world"
import { Menu } from "./menu"
import { ResourceSet } from "../resources/resourceSet"
import { createResource } from "../resources/resCreate"
import { ResourceType } from "../resources/resTypes"

export class Graphics{
    paused: boolean
    showPower: boolean

    lockedTile: Tile | undefined

    ancPos: TilePos

    private left: number
    private top: number

    public canvas: HTMLCanvasElement

    public margins = {
        left: 5,
        top: 5
    } as const satisfies Record<string, number>

    miniMapImageData?: HTMLImageElement

    context: MinduistryRenderingContext
    inputHandler: InputHandler
    menu: Menu
    ctx: CanvasRenderingContext2D
    
    constructor(canvas: HTMLCanvasElement, world: World){
        this.canvas = canvas
        canvas.height = TILES_ON_SCREEN * TILE_SIZE

        canvas.width = canvas.height * 1.8



        this.menu = new Menu(world)
        
        this.paused = false
        this.showPower = true

        this.lockedTile = undefined
        
        
        

        this.ancPos = {j: Math.floor(world.w / 2 - TILES_ON_SCREEN / 2) , i: Math.floor(world.h / 2 - TILES_ON_SCREEN / 2)}

        

        
        this.left = canvas.getBoundingClientRect().left;
        this.top = canvas.getBoundingClientRect().top
        
        this.ctx = canvas.getContext('2d')!
        
        this.inputHandler = new InputHandler(this)
        this.context = new MinduistryRenderingContext(this, this.inputHandler)

    }

    makeMiniMap(world: World, tilesize: number): HTMLImageElement{
        let canvas = document.createElement("canvas")
        canvas.width = tilesize * world.w
        canvas.height = tilesize * world.h
        
        var image = new Image()
        world.renderMiniMap(canvas.getContext("2d")!, tilesize)

        image.src  = canvas.toDataURL("image/png");
        return image
    }

    renderMinimap(graphics: Graphics, world: World, tilesize: number){
        const {ctx, inputHandler} = graphics
        if(!this.miniMapImageData){
            this.miniMapImageData = this.makeMiniMap(world, tilesize)
        }

        ctx.drawImage(this.miniMapImageData, 0, 0)
        
        ctx.strokeStyle = "black"
        ctx.rect(this.ancPos.j * tilesize, this.ancPos.i * tilesize, 16 * tilesize, 16 * tilesize)
        ctx.stroke()

    }

    static renderBar(ctx: CanvasRenderingContext2D, pos: ScreenPos, dimensions: Dimensions, progress: number, color: string, text: string | null, translate: boolean = false){
        const {x, y} = pos
        const {w: length, h: height} = dimensions
        ctx.save()
        if(text){
            ctx.font = "bold " + height * 2 / 3 + "px Arial"
            ctx.fillText(text, x, y)
            ctx.translate(0, height * 0.4)
        }
        ctx.fillStyle = color
        ctx.strokeStyle = "black"
        ctx.beginPath()
        ctx.rect(x, y, length * progress, height)
        ctx.fill()
        ctx.beginPath()
        ctx.rect(x, y, length, height)
        ctx.stroke()
        ctx.restore()
        if(translate) ctx.translate(0, height * 2.4)
    }

    static renderInfoText(ctx: CanvasRenderingContext2D, pos: ScreenPos, height: number, text: string, translate: boolean = false){
        const {x, y} = pos
        ctx.save()
        ctx.font = "bold " + height * 2 / 3 + "px Arial"
        ctx.fillText(text, x, y)
        ctx.restore()
        if(translate) ctx.translate(0, height)
    }

    static renderDescription(ctx: CanvasRenderingContext2D, pos: ScreenPos, height: number, text: string, translate: boolean = false){
        const {x, y} = pos
        ctx.save()
        ctx.font = height * 2 / 3 + "px Arial"
        ctx.fillText(text, x, y)
        ctx.restore()
        if(translate) ctx.translate(0, height)
    }

    static renderResourcePerMinute(ctx: CanvasRenderingContext2D, pos: ScreenPos, type: ResourceType, am: number, text: string = "", size: number = Graphics.RES_SIZE, translate: boolean = false){
        ctx.save()
        let {x, y} = pos
        ctx.textBaseline = 'middle'
        ctx.font = "bold " + size * 2 / 3 + "px Arial"
        if(text){
            ctx.fillText(text, x, y)
            y += size * 1.1
        }
        x += size * 0.5
        
        createResource(type).render(ctx, {x: x, y: y}, undefined, size)
        x += size * 0.8
        ctx.fillText(am + "/min", x, y)
        x += size * 1.5
        ctx.restore()
        if(translate) ctx.translate(0, size * (1.1 + 1.5))
    }

    static renderProductionProcess(ctx: CanvasRenderingContext2D, pos: ScreenPos, input: ResourceSet, output: ResourceSet, text: string = "", size: number = Graphics.RES_SIZE, translate: boolean = false){
        ctx.save()
        let {x, y} = pos
        ctx.textBaseline = 'middle'
        ctx.font = "bold " + size * 2 / 3 + "px Arial"
        if(text){
            ctx.fillText(text, x, y)
            y += size * 1.1
        }
        x += size * 0.5
        Resource.forEachType(type => {
            let am = input.get(type)
            if(!am) return
            createResource(type).render(ctx, {x: x, y: y}, undefined, size)
            x += size * 0.8
            ctx.fillText(am.toString(), x, y)
            x += size * 1.5
        })
        ctx.fillText(" → ", x, y)
        x += size * 2
        Resource.forEachType(type => {
            let am = output.get(type)
            if(!am) return
            createResource(type).render(ctx, {x: x, y: y}, undefined, size)
            x += size * 0.8
            ctx.fillText(am.toString(), x, y)
            x += size * 1.5
        })
        ctx.restore()
        if(translate) ctx.translate(0, size * (1.1 + 1.5))
    }

    static renderResourceSetPerMinute(ctx: CanvasRenderingContext2D, set: ResourceSet, pos: ScreenPos, size: number = Graphics.RES_SIZE, text?: string, translate: boolean = false){
        ctx.save()
        let {x, y} = pos
        ctx.textBaseline = 'middle'
        ctx.font = "bold " + size * 2 / 3 + "px Arial"
        if(text){
            ctx.fillText(text, x, y)
            y += size * 1.1
        }
        x += size * 0.5
        Resource.forEachType(type => {
            let am = set.get(type)
            if(!am) return
            createResource(type).render(ctx, {x: x, y: y}, undefined, size)
            x += size * 0.8
            ctx.fillText(am.toString(), x, y)
            x += size * 1.5
        })
        ctx.restore()
        if(translate) ctx.translate(0, size * (1.1 + 1.5))
    }

    static renderResourceSet(ctx: CanvasRenderingContext2D, set: ResourceSet, pos: ScreenPos, size: number = Graphics.RES_SIZE, text?: string, translate: boolean = false){
        ctx.save()
        let {x, y} = pos
        ctx.textBaseline = 'middle'
        ctx.font = "bold " + size * 2 / 3 + "px Arial"
        if(text){
            ctx.fillText(text, x, y)
            y += size * 1.1
        }
        x += size * 0.5
        Resource.forEachType(type => {
            let am = set.get(type)
            if(!am) return
            createResource(type).render(ctx, {x: x, y: y}, undefined, size)
            x += size * 0.8
            ctx.fillText(am.toString(), x, y)
            x += size * 1.5
        })
        ctx.restore()
        if(translate) ctx.translate(0, size * (1.1 + 1.5))
    }

    get boundingRect(): Rectangle{
        return new Rectangle(this.left, this.top, Graphics.TILE_SIZE * Graphics.MAP_WIDTH, Graphics.TILE_SIZE * Graphics.MAP_HEIGHT)
    }

    renderPosToTilePos(pos: ScreenPos): TilePos{
        return {
            j: Math.floor((pos.x - this.left) / Graphics.TILE_SIZE + this.ancPos.j),
            i: Math.floor((pos.y - this.top) / Graphics.TILE_SIZE + this.ancPos.i)
        }
    }

    static get TILE_SIZE(){
        return TILE_SIZE
    }

    static get H_TILE_SIZE(){
        return H_TILE_SIZE
    }

    static get RES_SIZE(){
        return RES_SIZE
    }

    static get H_RES_SIZE(){
        return H_RES_SIZE
    }

    static get FPS(){
        return 30
    }

    static get FPM(){
        return 1800
    }

    static get OFFSET(){
        return 5
    }

    static get MAP_WIDTH(){
        return 15
    }

    static get MAP_HEIGHT(){
        return 15
    }


    tilePosToScreenPos(pos: TilePos, tileSize: number = Graphics.TILE_SIZE): ScreenPos{
        return {
            x: 5 + (pos.j - this.ancPos.j) * tileSize,
            y: 5 + (pos.i - this.ancPos.i) * tileSize
        }
    }

    static tsize(x: number): number{
        return x * Graphics.TILE_SIZE
    }

    static showStepsAsTime(num: number): string{
        return this.showNumber(num / Graphics.FPS)
    }

    static milliesToSeconds(t: number): number{
        return Math.ceil(t / 100) / 10
    }

    static milliesToSteps(t: number): number{
        return Math.ceil(t * Graphics.FPS / 1000)
    }

    static invertMillieInterval(millies: number): string{
        let num = 1000 / millies
        let rounded = Math.ceil(num * 10 - 0.5) / 10
        if(Math.abs(num - rounded) < 0.05) return rounded + "/s"
        else return "~" + rounded + "/s"

    }

    static showNumberPerTime(num: number): string{
        let perSecond = num * Graphics.FPS
        return Graphics.showNumber(perSecond) + "/s"
    }

    static showNumber(num: number): string{
        if(num > 10000){ 
            return Math.floor(num / 1000 + 500) + "k"
        } else{
            return "" + Math.floor(num  * 10 + 0.5) / 10
        }
    }


}

export const TILES_ON_SCREEN = 15
//let w = 0.85 * Math.min(screen.width, screen.height)
let w = Math.min(window.innerWidth, window.innerHeight) - 20
let TILE_SIZE = Math.floor(w / TILES_ON_SCREEN) // PFUSCH!!!
let H_TILE_SIZE = 0.5 * TILE_SIZE
console.log(TILES_ON_SCREEN * TILE_SIZE)

const RES_SIZE = TILE_SIZE / 3
const H_RES_SIZE = TILE_SIZE / 6