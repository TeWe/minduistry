import { Rectangle, ScreenPos } from "../geo/geo"
import { Graphics } from "./graphics"
import { InputHandler } from "./inputHandler"

export class MinduistryRenderingContext{
    ctx: CanvasRenderingContext2D
    graphics: Graphics
    inputHandler: InputHandler
    

    

    constructor(graphics: Graphics, inputHandler: InputHandler){
        this.ctx = graphics.ctx,
        this.graphics = graphics
        this.inputHandler = inputHandler
    }

    absoluteScreenPos(pos: ScreenPos): ScreenPos{
        const matrix = this.ctx.getTransform()
        return {
            x: pos.x + matrix.e,
            y: pos.y + matrix.f
        }
    }

    private absoluteRect(translatedRect: Rectangle): Rectangle{
        const {x, y} = this.absoluteScreenPos({x: translatedRect.x0, y: translatedRect.y0})
        return new Rectangle(x, y, translatedRect.w, translatedRect.h)
    }

    mouseInRect(translatedRect: Rectangle, offMainScreen = false): boolean{
        const absoluteRect = this.absoluteRect(translatedRect)
        return this.inputHandler.mouseInRect(absoluteRect, offMainScreen)
    }

    onClickInRect(translatedRect: Rectangle, f: (pos: ScreenPos | undefined) => void, offMainScreen: boolean = false): void{
        const absoluteRect = this.absoluteRect(translatedRect)
        this.inputHandler.onClickInRect(absoluteRect, f, offMainScreen)
    }

    rect(rect: Rectangle): void{
        this.ctx.rect(rect.x0, rect.y0, rect.w, rect.h)
    }
}