


export function getRandomInt(max: number): number {
  return Math.floor(Math.random() * max);
}

export function getRandomFloatingPoint(from: number, to: number): number {
  return from + (to - from) * Math.random()
}

export function loadImage(path: string){
  let img = new Image()

  //img.src = "https://rudeirer.wtf/scripts/advent2023/minduistry/img/" + path
  img.src = "img/" + path
  return img
}

export function loadAudio(path: string){
  let img = new Audio()
  img.src = "sound/" + path
  return img
}

export function getRandomListElement<T>(list: T[]): T {
  return list[Math.floor(Math.random() * list.length)]
}

function shuffleList(list) {
  return list.sort(() => (Math.random() > 0.5) ? 1 : -1);
}

function distance(p1, p2) {
  return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
}

function l1Distance(p1, p2) {
  return Math.abs(p1.x - p2.x) + Math.abs(p1.y - p2.y)
}


export function proba<T>(...list: [number, T][]): T {
  let val = Math.random();
  let cumu = 0;
  for (let entry of list) {
    cumu += entry[0];
    if (cumu >= val) return entry[1];
  }
  return list[list.length - 1][1]
}

export function doWithProbaList(list: [number, () => void]): void {
  let outcome: () => void = proba(list);
  outcome();
}

export function doWithProba(prob: number, f: () => void): void {
  if(Math.random() < prob) f()
}

export function round(num: number){
  return Math.ceil(num * 10 - 0.5) / 10
}


class Vector{
  constructor(x, y){
    this.x = x
    this.y = y
  }

  scaleTo(len){
    let l2 = this.l2Norm
    if(l2 == 0) return
    this.x /= l2 / len
    this.y /= l2 / len
  }

  scaledBy(factor){
    return new Vector(this.x * factor, this.y * factor)
  }

  scaledTo(len){
    let l2 = this.l2Norm
    if(l2 == 0) return new Vector(0, 0)
    return new Vector(this.x / l2 * len, this.y / l2 * len)
  }

  normalize(){
    let l2 = this.l2Norm
    this.x /= l2
    this.y /= l2
  }

  add(pos){
    this.x += pos.x
    this.y += pos.y
  }

  to(pos){
    return new Vector(pos.x - this.x, pos.y - this.y)
  }

  get l1Norm(){
    return Math.abs(this.x) + Math.abs(this.y)
  }

  get l2Norm(){
    return Math.sqrt(this.x * this.x + this.y * this.y)
  }

  distanceTo(pos2){
    return Math.sqrt((this.x - pos2.x) * (this.x - pos2.x) + (this.y - pos2.y) * (this.y - pos2.y))
  }

  static distance(p1, p2){
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
  }

  static fromPos(pos){
    return new Vector(pos.x, pos.y)
  }

}
