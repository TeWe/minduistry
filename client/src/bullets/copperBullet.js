class CopperBullet{
    constructor(pos, targetPos){
        this.pos = pos
        this.targetPos = targetPos

        this.velo = new Vector(this.targetPos.x - this.pos.x, this.targetPos.y - this.pos.y)
        this.velo.scaleTo(0.2)

        this.deathCountDown = CountDown.millies(1500)

        this.radius = 0.2
        this.renderRadius = 0.2 * Graphics.TILE_SIZE
    }

    render(ctx){
        ctx.save()
        ctx.beginPath()
        ctx.arc(0, 0, this.renderRadius, 0, 2 * Math.PI)
        ctx.fillStyle = "orange"
        ctx.strokeStyle = "black"
        ctx.fill()
        ctx.stroke()
        ctx.restore()
    }

    step(world){
        this.deathCountDown.step()
        if(this.deathCountDown.completed) this.deathFlag = true
        this.pos.x += this.velo.x
        this.pos.y += this.velo.y   

        for(let unit of world.units){
            if(Vector.distance(this.pos, unit.pos) > this.radius + unit.radius) continue
            unit.takeDamage(5)
            this.deathFlag = true
            break
        }
    }
}