import { doWithProba, getRandomListElement, proba } from "../util"
import { BackType, Tile, TileType } from "./tile"
import { getDistributionData } from "./resourceDistribution"

export const enum IslandType{
    STARTER = "STARTER",
    MID = "MID",
    PANGEA = "PANGEA"
}





export class Island {
    tiles: Tile[]
    type: IslandType

    constructor(tiles: Tile[], type: IslandType) {
        this.tiles = tiles
        this.type = type
    }

    forAllTiles(f: (t: Tile) => void): void {
        this.tiles.forEach(f)
    }

    growResourcePatches() {
        const growthData = getDistributionData(this.type)

        let typeProbs: [number, TileType][] = []
        let sum = 0
        Object.values(TileType).forEach(type => {
            if (type == TileType.EMPTY) return
            const data = growthData[type]
            if (!data) return
            typeProbs.push([data.sparcity, type])
            sum += data.sparcity
        })

        // plant initial resources
        typeProbs.push([1 - sum, TileType.EMPTY])
        this.forAllTiles(tile => {
            if (tile.backType === BackType.WATER) return
            tile.type = proba(...typeProbs),
                tile.backType = proba([0.8, BackType.EMPTY], [0.2, BackType.SAND])
        })

        const counts: Partial<Record<TileType, number>> = {}
        for(let tile of this.tiles) counts[tile.type] = (counts[tile.type] ?? 0) + 1
        for(let type of Object.values(TileType)){
            const data = growthData[type]
            if(!data) continue
            for(let i= (counts[type] ?? 0); i < (data.minimum ?? 0); i++){
                const tile = getRandomListElement(this.tiles)
                tile.type = type
            }
            
            
        }

        // grow resource patches
        for (let round = 1; round <= 10; round++) {
            this.forAllTiles(tile => {
                if (tile.backType == BackType.WATER) return
                const type = tile.type
                const data = growthData[type]
                if (!data) return
                if (data.growthRounds < round) return
                tile.forAllNeighbours(nb => doWithProba(data.growthRate, () => {
                    if (nb.backType !== BackType.WATER) nb.type = tile.type
                }))
            })
        }
    }
}