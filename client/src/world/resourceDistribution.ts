import { start } from "repl"
import { IslandType } from "./island"
import { TileType } from "./tile"

export type DistrubutionData = {
    growthRounds: number
    growthRate: number
    sparcity: number
    minimum?: number
}

const starterDistribution: Partial<Record<TileType, DistrubutionData>> = {
    COPPER: {
        sparcity: 0.02,
        growthRate: 0.05,
        growthRounds: 5,
        minimum: 4
    },
    COAL: {
        sparcity: 0.01,
        growthRate: 0.2,
        growthRounds: 3,
        minimum: 2
    },
}

const defaultDistribution: Partial<Record<TileType, DistrubutionData>> = {
    COPPER: {
        sparcity: 0.02,
        growthRate: 0.05,
        growthRounds: 7,
        minimum: 4
    },
    COAL: {
        sparcity: 0.01,
        growthRate: 0.2,
        growthRounds: 3
    },
    SALAD: {
        sparcity: 0.005,
        growthRate: 0.01,
        growthRounds: 2
    },
    LEAD: {
        sparcity: 0.013,
        growthRate: 0.1,
        growthRounds: 3
    },
    MORKITE: {
        sparcity: 0.008,
        growthRate: 0.03,
        growthRounds: 2
    }
}

export function getDistributionData(type: IslandType): Partial<Record<TileType, DistrubutionData>> {
    switch(type){
        case IslandType.STARTER: return starterDistribution
        case IslandType.PANGEA: return defaultDistribution
        default: return defaultDistribution
    }
} 