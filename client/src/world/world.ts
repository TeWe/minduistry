import { Dimensions, ScreenPos, TilePos, tileDistance } from "../geo/geo"
import { Graphics } from "../graphics/graphics"
import { Menu } from "../graphics/menu"
import { Island, IslandType } from "./island"
import { Cost } from "../resources/cost"
import { createResource } from "../resources/resCreate"
import { ResourceType } from "../resources/resTypes"
import { Resource } from "../resources/resource"
import { ResourceStorage } from "../resources/resource_storage"
import { Base } from "../structures/base"
import { PowerNetwork } from "../structures/power/power_network"
import { Empty, Structure } from "../structures/structure"
import { doWithProba, proba } from "../util"
import { BackType, Tile, TileType } from "./tile"

export class World {

    globalStorage: ResourceStorage = new ResourceStorage(0, 0)
    networks: PowerNetwork[] = []
    islands: Island[] = []
    w: number = 0
    h: number = 0
 
    map: Tile[][] = []

    nonEmptyStructures: Structure[] = []

    menu?: Menu // ohje, das war pfuschig
    
    constructor() {
        this.init()
        this.buildEmptyMap(60, 60)
        this.buildPangeaIsland(50)
    }

    init(){
        this.globalStorage = new ResourceStorage(1000, 1000)
        this.globalStorage.add(ResourceType.COPPER, 50) // 100
        //this.globalStorage.add(ResourceType.COAL, 10) // 10
        this.networks = []
        this.islands = []
    }

    buildEmptyMap(w: number, h: number){
        this.w = w
        this.h = h
        this.map = []
        for (let i = 0; i < h; i++) {
            this.map.push([])
            for (let j = 0; j < w; j++){
                this.map[i].push(new Tile(this, { j: j, i: i }, TileType.EMPTY, BackType.WATER))
            }
        }
    }

    get tiles(): Tile[] {
        let res: Tile[] = []
        this.forAllTiles(t => res.push(t))
        return res
    }

    forAllTiles(f: (t: Tile) => void): void {
        for (let i = 0; i < this.h; i++) for (let j = 0; j < this.w; j++) {
            f(this.map[i][j])
        }
    }

    createIsland(x: number, y: number, r: number, type: IslandType, proba: number = 0.65): Island{
        type TileWrapper ={tile: Tile, in: boolean}
        const wrappedMap: TileWrapper[][] = this.map.map(line => line.map(t => ({tile: t, in: false})))


        const islandTiles: Tile[] = []

        let add = (tile: Tile) => {
            const wrapper = wrappedMap[tile.pos.i][tile.pos.j]
            if(wrapper.in) return
            islandTiles.push(wrapper.tile)
            wrapper.in = true
        }

        let midPos = {j: x, i: y}

        add(this.tileAtPos(midPos)!)

        this.place(midPos, new Base())

        for(let t=0; t<r; t++){
            let newTiles: Tile[] = []
            for(let tile of islandTiles) tile.forAllNeighbours(nb => doWithProba(proba, () => newTiles.push(nb)))
            for(let tile of newTiles) add(tile)
        }

        for(let tile of islandTiles) tile.backType = BackType.EMPTY

        const island = new Island(islandTiles, type)
        this.islands.push(island)
        island.growResourcePatches()
        return island
    }

    buildPangeaIsland(radius: number){
        const margin = {j: this.w - 2 * radius, i: this.h - 2*radius}
        const mid = {j: Math.floor(this.w / 2), i: Math.floor(this.h / 2)}
        this.createIsland(mid.j, mid.i, radius, IslandType.PANGEA, 0.35)

        const tile = this.tileAtPos(mid)
        tile?.forAllNeighbours(t => {
            t.backType = BackType.EMPTY
            t.type = TileType.EMPTY
        })
    }

    buildIslands(radius: number){
        const w = Math.floor(this.w / (2 * radius + 1))
        const h = Math.floor(this.w / (2 * radius + 1))

        for(let i=0; i<h; i++){
            for(let j=0; j<w; j++){
                const x = 1 + radius + j * (2 * radius + 1)
                const y = 1 + radius + i * (2 * radius + 1)
                let type = IslandType.MID
                if(i == 0 && j == 0 || i == h - 1 && j == w - 1) type = IslandType.STARTER
                 const island = this.createIsland(x, y, radius, type)
            }
        }
    }

    

    tileAtPos(pos: TilePos): Tile | undefined {
        let j = Math.floor(pos.j)
        let i = Math.floor(pos.i)
        if(!this.map[i]) return undefined
        return this.map[i][j]
    }

    structureAtPos(pos: TilePos): Structure | undefined {
        const tile = this.tileAtPos(pos)
        if (tile) return tile.structure
        else return new Empty()
    }

    removeStructure(str: Structure){
        str.forAllNeighbours(nb => {
            nb.onRemoveNeighbour(str)
        })

        if(str.powerComponent){
            for(let nbcomp of str.powerComponent.adj){
                nbcomp.disconnect(str.powerComponent)
            }
            PowerNetwork.recomputeNetworks(this)
        }

        this.nonEmptyStructures = this.nonEmptyStructures.filter(structure => structure !== str)
    }

    place(pos: TilePos, str: Structure) {
        const tile = this.map[pos.i][pos.j]
        let oldStr = tile.structure

        if(!(oldStr instanceof Empty)) this.removeStructure(oldStr)

        tile.structure = str
        str.placeAtPos(this, pos)
        str.location = {world: this, pos: pos, tile: tile}
        this.forAllStructures(nb => {
            if (nb.acceptsSource(str) && str.acceptsSink(nb)) {
                str.addSink(nb)
                nb.addSource(str)
            }
            if (str.acceptsSource(nb) && nb.acceptsSink(str)) {
                nb.addSink(str)
                str.addSource(nb)
            }
        })
        this.forAllStructures(nb => {
            nb.onAddNeighbour(str)
        })

        this.nonEmptyStructures.push(str)
    }

    

    /*
    getClosestUnit(pos: TilePos, f: (u: Unit) => boolean = (_: Unit) => true){
        let closest = undefined
        let distance = 1000
        this.units.forEach(unit => {
            if(!f(unit)) return
            let d = Vector.distance(unit.pos, pos)
            if(d >= distance) return
            distance = d
            closest = unit
        })
        return closest
    }
    */

    getClosestStructure(pos: TilePos, f: (str: Structure) => boolean = (str: Structure) => true){
        let structures = this.structures
        let closest: Structure | undefined = undefined
        let distance = 1000
        structures.forEach(str => {
            if(!f(str)) return
            let d = tileDistance(str.midPos, pos)
            if(d >= distance) return
            distance = d
            closest = str
        })
        return closest
    }


    forAllStructures(f: (str: Structure) => void): void {
        this.structures.forEach(f)
    }

    

    get structures(): Structure[] {
        return this.nonEmptyStructures
    }

    
    step(delta: number): void {
        let t = Date.now()
        this.networks.forEach(network => network.step())
        this.forAllStructures(str => str.step(delta))
        console.log("Time for tick: " + (Date.now() - t))
    }

    renderResourceBar(ctx: CanvasRenderingContext2D, pos: ScreenPos) {
        ctx.save()
        ctx.textBaseline = 'middle';
        ctx.font = "bold " + 0.3 * Graphics.TILE_SIZE + "pt Arial"

        let {x: x0, y: y0} = pos

        let x = x0 + 0.5 * Graphics.TILE_SIZE
        let y = y0
        Resource.forEachType(type => {
            if(this.globalStorage.get(type) == 0) return
            createResource(type).render(ctx, { x: x, y: y }, undefined, Graphics.RES_SIZE * 1.4)
            ctx.fillText(this.globalStorage.get(type).toString(), x + 0.5 * Graphics.TILE_SIZE, y)
            y += 0.5 * Graphics.TILE_SIZE
        })
        ctx.restore()
    }

    renderMiniMap(ctx: CanvasRenderingContext2D, tilesize: number){
        ctx.save()
        // render tiles
        for (let i = 0; i < this.h; i++) {
            for (let j = 0; j < this.w; j++) {
                this.tileAtPos({i, j})!.renderMinimapLayer(ctx, tilesize)
                ctx.translate(tilesize, 0)
            }   
            ctx.translate(-tilesize * this.w, tilesize)
        }
        ctx.translate(0, - tilesize * this.h)
        ctx.restore()
    }

    render(graphics: Graphics, pos0: TilePos, dims: Dimensions) {
        const {ctx} = graphics
        ctx.save()
        let {i: i0, j: j0} = pos0
        const {w, h} = dims
        let pos: {i: number, j: number} = {i: i0, j: j0}

        // render tiles
        for (pos.i = i0; pos.i < i0 + h; pos.i++) {
            for (pos.j = j0; pos.j < j0 + w; pos.j++) {
                const tile = this.tileAtPos(pos)
                if(!tile) continue
                tile.renderBottomLayer(graphics)
                ctx.translate(Graphics.TILE_SIZE, 0)
            }
            ctx.translate(-Graphics.TILE_SIZE * w, Graphics.TILE_SIZE)
        }
        ctx.translate(0, - Graphics.TILE_SIZE * h)
        
        for (pos.i = i0; pos.i < i0 + h; pos.i++) {
            for (pos.j = j0; pos.j < j0 + w; pos.j++) {
                const tile = this.tileAtPos(pos)
                if(!tile) continue
                tile.renderTopLayer(graphics)
                ctx.translate(Graphics.TILE_SIZE, 0)
            }
            ctx.translate(-Graphics.TILE_SIZE * w, Graphics.TILE_SIZE)
        }
        ctx.translate(0, - Graphics.TILE_SIZE * h)

        for (pos.i = i0; pos.i < i0 + h; pos.i++) {
            for (pos.j = j0; pos.j < j0 + w; pos.j++) {
                const tile = this.tileAtPos(pos)
                if(!tile) continue
                tile.renderUILayer(graphics)
                ctx.translate(Graphics.TILE_SIZE, 0)
            }
            ctx.translate(-Graphics.TILE_SIZE * w, Graphics.TILE_SIZE)
        }
        ctx.translate(0, - Graphics.TILE_SIZE * h)

        /*
        if(this.winningCountDown){
            ctx.beginPath()
            ctx.globalAlpha = 1 - this.winningCountDown.progress
            ctx.font = "bold " + 4 * Graphics.TILE_SIZE + "px Arial"
            ctx.fillStyle = "red"
            ctx.globalAlpha = 1 - this.nameCountDown.progress
            ctx.fillText("You win!", 0, Graphics.tsize(Graphics.MAP_HEIGHT / 2))
        }
        */

        ctx.restore()
    }

    isAffordable(costs: Cost): boolean {
        let affordable = true
        costs.forEach((type, value) => affordable = affordable && this.globalStorage.hasAtLeast(type, value))
        return affordable
    }

    spendResources(costs: Cost): void {
        costs.forEach((type, cost) => {
            this.globalStorage.remove(type, cost)
        })
    }
}

