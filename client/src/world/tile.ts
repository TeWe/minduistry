import { TilePos, tileDistance } from "../geo/geo"
import { Graphics } from "../graphics/graphics"
import { Structure, Empty } from "../structures/structure"
import { loadImage } from "../util"
import { World } from "./world"
import { Direction } from "../geo/direction"
import { PowerNode } from "../structures/power/power_node"
import { PowerNetwork } from "../structures/power/power_network"

const backgrounds = {
    coal: loadImage("textures/coal.png"),
    salad: loadImage("textures/salad.png"),
    lead: loadImage("textures/lead.png"),
    copper: loadImage("textures/copper.png"),
    empty: loadImage("background/empty.png"),
    sandy: loadImage("background/sandy.png"),
    morkite: loadImage("textures/morkite.png"),
    water: loadImage("background/water.png")

}

export enum TileType {
    EMPTY = "EMPTY",
    COAL ="COAL",
    COPPER ="COPPER",
    SALAD = "SALAD",
    LEAD = "LEAD",
    MORKITE = "MORKITE"
} 


export const enum BackType {
    EMPTY = "EMPTY",
    SAND = "SAND",
    WATER = "WATER",
}




export class Tile{
    private world: World
    public pos: TilePos
    private midPos: TilePos
    public structure: Structure
    public type: TileType
    public backType: BackType
    
    constructor(world: World, pos: TilePos, type: TileType, backType: BackType){
        this.world = world
        this.pos = pos
        this.midPos = {i: this.pos.i + 0.5, j: this.pos.j + 0.5}
        this.type = type
        this.backType = backType
        this.structure = new Empty()
    }

    renderUILayer(graphics: Graphics): void{
        this.structure.renderUILayer(graphics)
    }

    getMinimapColor(): string{
        switch(this.type){
            case "SALAD": return "green"
            case "COPPER": return "orange"
            case "LEAD": return "purple"
            case "MORKITE": return "green"
            case "COAL": return "rgb(50, 50, 50"
        }

        switch(this.backType){
            case BackType.SAND:
            case BackType.EMPTY: return "rgb(135, 112, 47)"
            case BackType.WATER: return "blue"
        } 
    }

    renderMinimapLayer(ctx: CanvasRenderingContext2D, tilesize: number): void{
        ctx.fillStyle  = this.getMinimapColor()
        ctx.beginPath()
        ctx.fillRect(0, 0, tilesize, tilesize)
    }

    renderBottomLayer(graphics: Graphics): void{
        let {ctx, inputHandler, menu} = graphics
        ctx.save()

        switch(this.backType){
            case "SAND":
                ctx.drawImage(backgrounds.sandy, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
            case "EMPTY": 
                ctx.drawImage(backgrounds.empty, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break;
            default:
                ctx.drawImage(backgrounds.water, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
        }        

        switch(this.type){
            case "COAL":
                ctx.drawImage(backgrounds.coal, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
            case "SALAD":
                ctx.drawImage(backgrounds.salad, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
            case "COPPER":
                ctx.drawImage(backgrounds.copper, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
            case "LEAD":
                ctx.drawImage(backgrounds.lead, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
            case "MORKITE":
                ctx.drawImage(backgrounds.morkite, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                break
        }

        /*
        if(this.blocked){
            ctx.save()
            ctx.globalAlpha = 0.2
            ctx.beginPath()
            ctx.rect(0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
            ctx.fillStyle = "red"
            ctx.fill()
            ctx.restore()
        }
        */

        this.structure.renderBottomLayer(graphics)

        inputHandler.onRightClickInTile(this, () => {
            if(this.structure && this.structure.team && !(this.structure instanceof Empty)){
                this.world.removeStructure(this.structure)
                this.world.place(this.pos, new Empty())
            }
        })

        inputHandler.onClickInTile(this, () => {
            if(menu.renderStr){
                if(menu.renderStr.canBeBuiltOn(this)){
                    let str = menu.lockedMenuItem!.generator()
                    for(let i=0; i<menu.rotationState; i++) str.rotate(1)
                    this.world.place(this.pos, str)
                    this.world.spendResources(menu.renderStr.costs)
                    //Menu.renderStr = Menu.lockedMenuItem.gen()
                    if(!this.world.isAffordable(menu.renderStr.costs)){
                        menu.deselectRenderStr()
                        menu.lockedMenuItem = undefined
                    }
                }
            } 
            else if(graphics.lockedTile == this) graphics.lockedTile = undefined
            else if(this.structure && this.structure.powerComponent
                    && graphics.lockedTile && graphics.lockedTile.structure 
                    && graphics.lockedTile.structure instanceof PowerNode){
                        let node = graphics.lockedTile.structure
                        if(node.powerComponent!.adj.includes(this.structure.powerComponent)){
                            this.structure.powerComponent.disconnect(node.powerComponent!)
                        } else if(tileDistance(node.midPos, this.structure.midPos) <= node.radius){
                            this.structure.powerComponent.connect(node.powerComponent!)
                        }
                        PowerNetwork.recomputeNetworks(this.world) 
                }
            else if(this.structure){
                    graphics.lockedTile = this
            }
        })

        ctx.restore()
        
    }

    renderTopLayer(graphics: Graphics){
        const {ctx, inputHandler, menu} = graphics
        this.structure.renderTopLayer(graphics)

        //if(this.world.tileAtPos(ctx.graphics.mouseTilePos) == this){
        if(inputHandler.mouseInTile(this)){
            if(menu.renderStr && menu.renderStr.canBeBuiltOn(this)){
                ctx.globalAlpha = 0.5
                menu.renderStr.renderBottomLayer(graphics)
                ctx.globalAlpha = 1
            } else{
                ctx.save()
                ctx.beginPath()
                ctx.rect(0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                ctx.lineWidth = 4
                ctx.stroke()
                ctx.restore()

                
                //this.structure.renderStructInfo(ctx)
                
            }
            
        }
    }

    forAllNeighbours(f: (t: Tile) => void){
        this.neighbours.forEach(f)
    }

    get neighbours(): Tile[] {
        return [
            this.getNeighbour(Direction.UP),
            this.getNeighbour(Direction.DOWN),
            this.getNeighbour(Direction.LEFT),
            this.getNeighbour(Direction.RIGHT)
        ].filter(t => t) as Tile[]
    }

    getNeighbour(dir: Direction): Tile | undefined{
        let pos = this.pos
        switch(dir){
            case Direction.UP:
                if(pos.i == 0) return undefined
                return this.world.tileAtPos({i: pos.i - 1, j: pos.j})
            case Direction.DOWN:
                if(pos.i >= this.world.h - 1) return undefined
                return this.world.tileAtPos({i: pos.i + 1, j: pos.j})
            case Direction.LEFT:
                if(pos.j == 0) return undefined
                return this.world.tileAtPos({i: pos.i, j:pos.j - 1})
            case Direction.RIGHT:
                if(pos.j >= this.world.w - 1) return undefined
                return this.world.tileAtPos({i: pos.i, j: pos.j + 1})
        }
    }
}

