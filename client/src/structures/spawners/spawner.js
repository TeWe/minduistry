class Spawner extends Structure{
    constructor(){
        super()
        this.units = []
    }

    get unitCap(){
        return 3
    }

    tryToSpawnUnit(){
        if(this.units.length >= this.unitCap) return false
        let unit = this.getSpawn(this.midPos, this.world)
        unit.homeStructure = this
        this.world.units.push(unit)
        this.units.push(unit)
        this.onSpawn(unit)
        return true
    }

    onSpawn(unit){}

    get spawnTime(){
        return 120
    }
   
    get name(){
        return "Spawner"
    }

    isSolid(){
        return false
    }
    
    canBeBuiltOn(tile){
        return true
    }

    onRemoveNeighbour(nb) {}

    renderAdditionalStructInfo(ctx){
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.spawnCountdown.progress, "red", "Spawnvorgang:", true)
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.radarCountDown.progress, "blue", "Nächster Angriff:")
    }
}
