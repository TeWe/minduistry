class AlexSpawner extends Spawner{
    constructor(){
        super()
        this.radarCountDown = new CountDown(this.attackTime, () => this.searchForTarget(), true)
        this.spawnCountdown = new CountDown(this.spawnTime, () => this.tryToSpawnUnit(), true)
    }

    get attackRadius(){
        return 10
    }

    renderUILayer(ctx){
        if(!ctx.mouseInTile(this.tile)) return
        ctx.save()
        ctx.beginPath()
        ctx.arc(Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE, this.attackRadius * Graphics.TILE_SIZE, 0, 2 * Math.PI)
        ctx.globalAlpha = 0.1
        ctx.fillStyle = "red"
        ctx.fill()
        ctx.restore()
    }


     // searches for enemy structure to attack
     searchForTarget(){
        let str = this.world.getClosestStructure(this.midPos, str => (!(str instanceof Empty)) && str.team != this.team)
        
        if(str && Vector.distance(str.midPos, this.midPos) <= this.attackRadius){
            this.units.forEach(a => {
                a.receiveAttackCommand(str)
            })
            return true
        } else return true
    }

    onSpawn(unit){}

    step(){
        this.spawnCountdown.step()
        this.radarCountDown.step()
     }
    
    get name(){
        return "Alex-Spawner"
    }

    get team(){
        return false
    }

    get spawnTime(){
        return 30000
    }

    get atk(){
        return 4
    }

    get attackTime(){
        return 60000
    }

    getSpawn(midPos, world){
        return new Alex(midPos, world)
    }

    renderBottomLayerUnrotated(ctx){
        ctx.drawImage(AlexSpawner.img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    renderAdditionalStructInfo(ctx){
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.spawnCountdown.progress, "red", "Spawnvorgang:", true)
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.radarCountDown.progress, "blue", "Nächster Angriff:")
    }
}
AlexSpawner = loadImage("structures/spawn/alex.png")