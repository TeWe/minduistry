class FriendlySpawner extends Spawner{
    constructor(){
        super()
        this.prodCost = this.getProductionCost()
        this.spawnCountDown = new CountDown(this.spawnTime, () => {
            
            if(!this.prodCost.isPaid()) return false
            this.prodCost = this.getProductionCost()
            return this.tryToSpawnUnit()
        }, true)
    }

    getProductionCost(){
        return new Cost()
    }

    get team(){
        return true
    }

    step(){
        if(this.prodCost.isPaid()) this.spawnCountDown.step()
    }

    onSpawn(unit){
        let str = world.getClosestStructure(this.midPos, s => s.team != this.team)
        unit.receiveAttackCommand(str)
    }

    getSpawn(midPos, world){
        return new Pascal(midPos, world)
    }

    get spawnTime(){
        return 200
    }

    acceptsSource(nb) {
        return this.neighbours.includes(nb)
    }

    addSource(nb) {
        
    }

    canReceive(from, res) {
        return this.prodCost[res.resType] > 0
    }

    receive(from, res){
        this.prodCost.pay(res.resType, 1)
    }

    renderAdditionalStructInfo(ctx){
        Graphics.renderResourceSet(ctx, this.prodCost, 0, 0, Graphics.H_TILE_SIZE, "Benötigte Resourcen", true)
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.spawnCountDown.progress, "green", "Spawnvorgang:", true)
    }
}