class PascalSpawner extends FriendlySpawner{
    get name(){
        return "Pascal-Spawner"
    }

    get text(){
        return "Erzeugt Pascals. "
    }

    get spawnTime(){
        return 2000
    }

    get team(){
        return true
    }

    getSpawn(midPos, world){
        return new Pascal(midPos, world)
    }

    renderBottomLayerUnrotated(ctx){
        ctx.drawImage(PascalSpawner.img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get costs(){
        return new Cost([ResourceType.COIN, 2], [ResourceType.COCAINE, 1])
    }

    getProductionCost(){
        return new Cost([ResourceType.SILICON, 20], [ResourceType.COPPER, 30])
    }
}
PascalSpawner.img = loadImage("structures/spawn/pascal2.png")