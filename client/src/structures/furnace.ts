import { Graphics } from "../graphics/graphics";
import { Cost } from "../resources/cost";
import { ResourceType } from "../resources/resTypes";
import { Resource } from "../resources/resource";
import { ResourceStorage } from "../resources/resource_storage";
import { loadImage } from "../util";
import { Tile } from "../world/tile";
import { PowerComponentType } from "./power/power_component";
import { Structure } from "./structure";
import { CountDown } from "./util/countdown";
import { FluidTask } from "./util/fluid_task";

export class Furnace extends Structure{
    storage: ResourceStorage
    fuelInserted: boolean
    burnCountDown: CountDown
    producer: FluidTask


    
    constructor(){
        super()
        this.storage = new ResourceStorage(5, 5)
        this.fuelInserted = false
        this.burnCountDown = CountDown.timesPerMinute(this.burnsPerMinue, () => {this.fuelInserted = false; return true}, true)
        this.addPowerComponent(PowerComponentType.SOURCE, 40)
        this.producer = new FluidTask(this.powerPerMinute, am => this.powerComponent!.produce(am) )
    }

    canBeBuiltOn(tile: Tile): boolean {
        return true
    }

    get powerPerMinute(){
        return 40
    }

    get burnsPerMinue(){
        return 15
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx
        if(this.fuelInserted)ctx.drawImage(IMG_ON, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        else ctx.drawImage(IMG_OFF, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name(){
        return "Kupferofen"
    }

    get text(){
        return "Verbrennt Kohle und erzeugt Strom"
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 45])
    }

    step(delta: number){
        
        if(this.fuelInserted){
                this.burnCountDown.step(delta)
                this.producer.step()
        }
        else if(this.storage.hasAtLeast(ResourceType.COAL) && this.network) {
            this.storage.remove(ResourceType.COAL)
            this.fuelInserted = true
        }
    }

    acceptsSource(str: Structure){
        return this.neighbours.includes(str)
    }

    addSource(str: Structure){
        
    }

    canReceive(from: Structure, res: Resource){
        return res.resType == ResourceType.COAL && this.storage.hasSpaceFor(ResourceType.COAL)
    }

    receive(from: Structure, res: Resource){
        this.storage.add(res.resType)
    }

    renderAdditionalStructInfo(graphics: Graphics) {
        //Graphics.renderResourceSet(ctx, this.prodCost, 0, 0, Graphics.H_TILE_SIZE, "Benötigte Resourcen", true)
        const ctx = graphics.ctx
        Graphics.renderResourceSet(ctx, this.storage, {x: 0, y: 0}, Graphics.H_TILE_SIZE, "Lager: ", true)
        Graphics.renderResourcePerMinute(ctx, {x: 0, y: 0}, ResourceType.COAL, this.burnsPerMinue, "max. Verbrauch:", Graphics.H_TILE_SIZE, true)

        Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, this.burnCountDown.progress, "orange", "Verbrennung", true)
        Graphics.renderInfoText(ctx, {x: 0, y: 0}, Graphics.H_TILE_SIZE, "max. Strom: " + this.powerPerMinute + " Watt", true)

    }
}

const IMG_OFF = loadImage("structures/furnace_front.png")
const IMG_ON = loadImage("structures/furnace_front_on.png")

