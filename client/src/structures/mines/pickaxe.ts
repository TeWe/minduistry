import { Graphics } from "../../graphics/graphics"
import { Coal } from "../../resources/coal"
import { Copper } from "../../resources/copper"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { loadAudio, loadImage } from "../../util"
import { Tile, TileType } from "../../world/tile"
import { Mine } from "./mine"

export class Pickaxe extends Mine{
    constructor(){
        super()
    }


    get costs(){
        return new Cost([ResourceType.COPPER, 20])
    }

    get name(){
        return "Mine"
    }

    get text(){
        return "Baut Kupfer oder Kohle ab"
    }

    playMiningSound(){
        AUDIO.play()
    }

    adjustToTile(){
        switch(this.location!.tile.type){
            case TileType.COAL:
                this.produce = () => new Coal()
                this.yieldPerMinute = 10
                break;
            case TileType.COPPER:
                this.produce = () => new Copper()
                this.yieldPerMinute = 15
                break;
        }
    }

    canBeBuiltOn(tile: Tile){
        return tile.type == TileType.COAL || tile.type == TileType.COPPER
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    
}

const IMG = loadImage("structures/mines/pickaxe_copper.png")
const AUDIO = loadAudio("structures/pickaxe.mp3")