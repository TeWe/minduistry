import { Graphics } from "../../graphics/graphics"
import { Coal } from "../../resources/coal"
import { Resource } from "../../resources/resource"
import { Structure } from "../structure"
import { CountDown } from "../util/countdown"
import { PerMinuteCounter } from "../util/per_minute_counter"
import { Rotator } from "../util/rotator"

export abstract class Mine extends Structure{
    mining: number
    sinks: Structure[]
    rotator?: Rotator<Structure>
    pmCounter: PerMinuteCounter

    miningCountDown: CountDown

    yieldPerMinute: number = 60

    constructor(){
        super()
        this.mining = 0
        this.sinks = []

        this.rotator = undefined
        this.pmCounter = new PerMinuteCounter()

        this.miningCountDown = CountDown.dummy()
    }

    private restartMiningCountDown(): void{
        this.miningCountDown = CountDown.timesPerMinute(this.yieldPerMinute, () => this.mine())
    }


    abstract adjustToTile(): void
    

    onPlacement(){
        this.adjustToTile()
        this.restartMiningCountDown()
    }

    /**
     * This should always be overriden, but typescript wants something here
     * @returns 
     */
    produce(): Resource {
        return new Coal()
    }

    mine(){
        if(!this.rotator) return false
        let coal = this.produce()

        let erg = false
        this.rotator.doForNext(sink => {
            if(!sink.canReceive(this, coal)) return false
            sink.receive(this, coal)
            erg = true
            this.pmCounter.register()
            //this.playMiningSound()
            return true
        })
        return erg
    }

    playMiningSound(){}

    step(delta: number){
        this.miningCountDown.step(delta)
        this.pmCounter.step(delta)
    }

    addSink(str: Structure){
        this.sinks.push(str)
        this.rotator = new Rotator(this.sinks)
    }

    acceptsSink(nb: Structure){
        return this.neighbours.includes(nb)
    }

    onRemoveNeighbour(nb: Structure){
        this.sinks = this.sinks.filter(s => s != nb)
        if(this.sinks.length > 0) this.rotator = new Rotator(this.sinks)
        else this.rotator = undefined
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, this.miningCountDown.progress, "black", "Minierungsfortschritt:", true)
        Graphics.renderResourcePerMinute(ctx, {x: 0, y: 0}, this.produce().resType, this.yieldPerMinute, "max. Ertrag", Graphics.H_TILE_SIZE , true)
        Graphics.renderResourcePerMinute(ctx, {x: 0, y: 0}, this.produce().resType, this.pmCounter.query(), "tats. Ertrag (grob gemittelt)", Graphics.H_TILE_SIZE , true)

    }

}