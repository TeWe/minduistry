import { Graphics } from "../../graphics/graphics"
import { PowerComponentType } from "../power/power_component"
import { Mine } from "./mine"

export abstract class ElectricMine extends Mine{
    constructor(){
        super()
        this.addPowerComponent(PowerComponentType.SINK, this.powerDemand)
    }

    step(){
        if(this.powerComponent!.drainStorageBy(this.powerDemand))
            this.miningCountDown.step()
    }

    get powerDemand(){
        return 40
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        super.renderAdditionalStructInfo(graphics)
        Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, this.network?.efficiency.val!, "yellow", "Benötigter Strom: " + Graphics.showNumberPerTime(this.powerDemand)) 
    }
}