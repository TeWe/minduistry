import { Graphics } from "../../graphics/graphics";
import { Cost } from "../../resources/cost";
import { ResourceType } from "../../resources/resTypes";
import { Salad } from "../../resources/salad";
import { loadImage } from "../../util";
import { Tile, TileType } from "../../world/tile";
import { Mine } from "./mine";

export class Garden extends Mine{
    get costs(){
        return new Cost([ResourceType.COPPER, 45])
    }

    get name(){
        return "Garten"
    }

    adjustToTile(){
        switch(this.location!.tile.type){
            case TileType.SALAD:
                this.produce = () => new Salad()
                this.yieldPerMinute = 20
                break;
        }
    }

    canBeBuiltOn(tile: Tile){
        return tile.type == TileType.SALAD
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }
}

const IMG = loadImage("structures/mines/garden.png")