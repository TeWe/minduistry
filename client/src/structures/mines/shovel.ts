import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Sand } from "../../resources/sand"
import { loadImage } from "../../util"
import { BackType, Tile } from "../../world/tile"
import { Mine } from "./mine"

export class Shovel extends Mine{
    get costs(){
        return new Cost([ResourceType.COPPER, 15])
    }

    get name(){
        return "Grabegrube"
    }

    get text(){
        return "Erzeugt Sand."
    }

    adjustToTile(){
        switch(this.location!.tile.backType){
            case BackType.SAND:
                this.produce = () => new Sand()
                this.yieldPerMinute = 20
                break;
        }
    }

    canBeBuiltOn(tile: Tile){
        return tile.backType == BackType.SAND
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }
}

const IMG = loadImage("structures/mines/shovel_copper.png")