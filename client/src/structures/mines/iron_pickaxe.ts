import { Graphics } from "../../graphics/graphics"
import { Coal } from "../../resources/coal"
import { Copper } from "../../resources/copper"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { loadImage } from "../../util"
import { Tile, TileType } from "../../world/tile"
import { Mine } from "./mine"

export class IronPickaxe extends Mine{
    constructor(){
        super()
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 20])
    }

    get name(){
        return "Eisenmine"
    }

    get description(){
        return "Hier greift das Ofenfabrik-Prinzip."
    }

    adjustToTile(){
        switch(this.location!.tile.type){
            case TileType.COAL:
                this.produce = () => new Coal()
                this.yieldPerMinute = 15
                break;
            case TileType.COPPER:
                this.produce = () => new Copper()
                this.yieldPerMinute = 20
                break;
        }
    }

    canBeBuiltOn(tile: Tile){
        return tile.type == TileType.COAL || tile.type == TileType.COPPER
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }
}

const IMG = loadImage("structures/mines/pickaxe_iron.png")