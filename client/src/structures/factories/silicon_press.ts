import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { ResourceSet } from "../../resources/resourceSet"
import { Silicon } from "../../resources/silicon"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { ElectricFactory } from "./electric_factory"

export class SiliconPress extends ElectricFactory{
    

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name(){
        return "Silikonpresse"
    }

    get text(){
        return "Erzeugt Silikon (benötigt Strom)"
    }

    get demand(){
        return 50
    }

    get productionTime(){
        return 100
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 60], [ResourceType.COAL, 15])
    }

    canBeBuiltOn(tile: Tile){
        return true
    }

    generateProduct(){
        return new Silicon()
    }

    getBasicProductionCost(){
        return new Cost([ResourceType.COAL, 3], [ResourceType.SAND, 5])
    }

    getBasicProductionResult(){
        return new ResourceSet([ResourceType.SILICON, 1])
    }
    
}

const IMG = loadImage("structures/factories/silicon2.png")