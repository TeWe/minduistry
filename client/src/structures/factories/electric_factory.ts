import { Graphics } from "../../graphics/graphics"
import { PowerComponentType } from "../power/power_component"
import { FluidTask } from "../util/fluid_task"
import { Factory } from "./factory"

export abstract class ElectricFactory extends Factory{
    private powerConsumptionTask: FluidTask
    
    constructor(){
        super()
        this.addPowerComponent(PowerComponentType.SINK, 2 * this.powerDemand)
        this.powerConsumptionTask = new FluidTask(this.powerDemand, (am) => this.powerComponent!.setDemand(am))
    }

    step(){
        if(!this.powerComponent) return
        this.powerConsumptionTask.step()
        if(this.getInternalProdCost().isPaid() && this.powerComponent.providesPower)
            this.prodCooldown.step()
    }
    
    get powerDemand(): number{
        return 100
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        super.renderAdditionalStructInfo(graphics)
        let network = this.powerComponent!.network
        if(!network){
            Graphics.renderInfoText(ctx, {x: 0, y: 0}, 0.5 * Graphics.TILE_SIZE, "Momentan an kein Stromnetz angeschlossen", true)
        }
        else{
            Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, network.efficiency.val, "yellow", "Benötigter Strom: " + this.powerDemand + " Watt")
        }
    }
}