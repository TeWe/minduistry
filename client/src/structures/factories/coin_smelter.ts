import { Graphics } from "../../graphics/graphics"
import { Coin } from "../../resources/coin"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { ResourceSet } from "../../resources/resourceSet"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Factory } from "./factory"

export class CoinSmelter extends Factory{
    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name(){
        return "Münzprägerei"
    }

    get description(){
        return "Schmilzt Kuper zu Kupfermünzen"
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 30])
    }

    get productionTime(){
        return 100
    }

    canBeBuiltOn(tile: Tile){
        return true
    }

    generateProduct(){
        return new Coin()
    }

    getProductionCost(){
        return 
    }

    protected getBasicProductionCost(): Cost {
        return new Cost([ResourceType.COPPER, 20], [ResourceType.COAL, 5])
    }
    protected getBasicProductionResult(): ResourceSet {
        return new ResourceSet([ResourceType.COIN, 1])
    }
    
}

const IMG = loadImage("structures/factories/smelter.png")