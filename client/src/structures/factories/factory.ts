import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { ResourceSet } from "../../resources/resourceSet"
import { Structure } from "../structure"
import { CountDown } from "../util/countdown"
import { Rotator } from "../util/rotator"

export abstract class Factory extends Structure { 
    sinks: Structure[]
    rotator?: Rotator<Structure>

    prodCyclesPerMinute: number


    prodCooldown: CountDown = CountDown.seconds(1000, () => false)

    inputPerMinute: ResourceSet = new ResourceSet()
    outputPerMinute: ResourceSet = new ResourceSet()

    private prodCost: Cost = new Cost()
    private prodRes: ResourceSet = new ResourceSet()
    
    constructor() {
        super()
        this.sinks = []
        this.rotator = new Rotator([])
        this.prodCost = this.getBasicProductionCost()
        this.prodRes = this.getBasicProductionResult()
        this.prodCyclesPerMinute = this.basicProdCyclesPerMinute
        this.recalculatePerMinute()
        this.restartProductionProcess()

    }

    protected getInternalProdCost(): Readonly<Cost>{
        return this.prodCost
    }

    restartProductionProcess(): void{
        this.prodCooldown = CountDown.timesPerMinute(this.prodCyclesPerMinute, () => this.produce(), true)
    }

    private recalculatePerMinute(){
        this.inputPerMinute = new ResourceSet()
        this.outputPerMinute = new ResourceSet()

        this.prodCost.forEach((type, am) => this.inputPerMinute.add(type, am * this.prodCyclesPerMinute))
        this.prodRes.forEach((type, am) => this.outputPerMinute.add(type, am * this.prodCyclesPerMinute))
    }

    protected abstract getBasicProductionCost(): Cost
    protected abstract getBasicProductionResult(): ResourceSet
    protected abstract generateProduct(): Resource

    updateProdCost(prodCost: Cost): void{
        this.prodCost = prodCost
        this.recalculatePerMinute()
    }

    updateProdRes(prodRes: ResourceSet): void{
        this.prodRes = prodRes
        this.recalculatePerMinute()
    }

    updateProdCyclesPerMinute(pcpm: number){
        this.prodCyclesPerMinute = pcpm
        this.recalculatePerMinute()
    }

    get basicProductionCost(): Cost {
        return new Cost([ResourceType.COAL, 5])
    }

    get basicProductionResult() {
        return new ResourceSet([ResourceType.COPPER, 1])
    }

    get basicProdCyclesPerMinute(){
        return 10
    }


    produce(): boolean {
        if(!this.rotator) return false
        let product = this.generateProduct()
        
        let ejected = this.rotator.doForNext(sink => {
            if (!sink.canReceive(this, product)) return false
            sink.receive(this, product)
            return true
        })

        if(ejected) this.prodCost = this.getBasicProductionCost()
        return ejected
    }



    // TODO
    step(delta: number) {
        if(this.prodCost.isPaid()) this.prodCooldown.step(delta)
    }

    canReceive(from: Structure, res: Resource) {
        return this.prodCost.hasAtLeast(res.resType, 1)
    }

    receive(from: Structure, res: Resource){
        this.prodCost.pay(res.resType, 1)
    }

    addSink(str: Structure) {
        this.sinks.push(str)
        this.rotator = new Rotator(this.sinks)
    }

    acceptsSink(nb: Structure) {
        return this.neighbours.includes(nb)
    }

    acceptsSource(nb: Structure) {
        return this.neighbours.includes(nb)
    }

    addSource(nb: Structure) {
        
    }

    onRemoveNeighbour(nb: Structure) {
        this.sinks = this.sinks.filter(s => s != nb)
        if (this.sinks.length > 0) this.rotator = new Rotator(this.sinks)
        else this.rotator = undefined
    }

    renderAdditionalStructInfo(graphics: Graphics) {
        const ctx = graphics.ctx
        Graphics.renderProductionProcess(ctx, {x: 0, y: 0}, this.inputPerMinute, this.outputPerMinute, "Produktion pro Minute", Graphics.H_TILE_SIZE, true)

        Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, this.prodCooldown.progress, "green", "Produktion:", true)
    }
}