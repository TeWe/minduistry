import { Graphics } from "../../graphics/graphics"
import { Cocaine } from "../../resources/cocaine"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { ResourceSet } from "../../resources/resourceSet"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Factory } from "./factory"

export class Dealer extends Factory{
    
    generateProduct(){
        return new Cocaine()
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name(): string{
        return "Drogendealer"
    }

    get text(): string{
        return "Verkauft Kokain."
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 5])
    }

    protected getBasicProductionCost(): Cost {
        return new Cost([ResourceType.COPPER, 5])
    }
    protected getBasicProductionResult(): ResourceSet {
        return new Cost([ResourceType.COCAINE, 1])
    }

    get productionTime(){
        return 100
    }
}

const IMG = loadImage("structures/factories/dealer.png")