import { TilePos } from "../geo/geo"
import { Direction, clockwise, counterClockwise, forAllDirections } from "../geo/direction"
import { Graphics } from "../graphics/graphics"
import { Cost } from "../resources/cost"
import { World } from "../world/world"
import { Tile } from "../world/tile"
import { Resource } from "../resources/resource"
import { PowerComponent, PowerComponentType } from "./power/power_component"
import { PowerNetwork } from "./power/power_network"

type Rotation = 0 | 1 | 2 | 3

export type Transform = {
    rotValue: Rotation,
    mirrorValue: boolean
}

export abstract class Structure {
    
    transform: Transform 
    
   

    hp: number

    location?: {
        world: World,
        tile: Tile,
        pos: TilePos
    }

    dirs: Record<Direction, Direction>

    constructor() {
        this.transform = {
            rotValue: 0,
            mirrorValue: false
        }
        
        this.dirs = {
            [Direction.LEFT]: Direction.LEFT,
            [Direction.UP]: Direction.UP,
            [Direction.DOWN]: Direction.DOWN,
            [Direction.RIGHT]: Direction.RIGHT
        }
        this.hp = this.maxHP
    }

    get midPos(): TilePos{
        const pos: TilePos = this.location!.pos
        return {i: pos.i + 0.5, j: pos.j + 0.5}
    }

    get maxHP(): number{
        return 100
    }

    powerComponent?: PowerComponent

    step(delta: number) { }

    placeAtPos(world: World, pos: TilePos): void{
        this.location = {
            world: world,
            pos: pos,
            tile: world.tileAtPos(pos)!
        }
        this.onPlacement()
    }

    protected onPlacement(){
    }

    getNeighbour(dir: Direction): Structure {
        if (!this.location) throw "oh no!"
        let nbt = this.location.tile.getNeighbour(dir)
        if (!nbt || !nbt.structure) return new Empty()
        return nbt.structure
    }


    get costs(){
        return new Cost()
    }

    get name() {
        return "UNKNOWN"
    }

    render(ctx: CanvasRenderingContext2D) { }

    get neighbours(): Structure[] {
        return [
            this.getNeighbour(Direction.UP),
            this.getNeighbour(Direction.DOWN),
            this.getNeighbour(Direction.LEFT),
            this.getNeighbour(Direction.RIGHT)
        ]
    }

    forAllNeighbours(f: (str: Structure) => void) {
        this.neighbours.forEach(f)
    }

    canReceive(from: Structure, res: Resource): boolean {
        return false
    }

    receive(from: Structure, res: Resource, remainingDelta: number = 0): void {

    }

    get world(): World | undefined{
        return this.location?.world
    }

    get atk(): number{
        return 0
    }

    isAdjacent(from: Structure): boolean {
        return this.neighbours.includes(from)
    }

    rotate(deltaY: number): void {
        //if(this.mirrorValue) deltaY *= -1
        if (deltaY < 0) {
            this.transform.rotValue = (this.transform.rotValue + 3) % 4 as Rotation
            forAllDirections(dir => {
                this.dirs[dir] = clockwise(this.dirs[dir])
            })
        } else {
            this.transform.rotValue = (this.transform.rotValue + 1) % 4 as Rotation
            forAllDirections(dir => {
                this.dirs[dir] = counterClockwise(this.dirs[dir])
            })
        }
    }

    get network(): PowerNetwork | undefined{
        return this.powerComponent?.network
    }

    mirrorVertically(): void{
        this.transform.mirrorValue = !this.transform.mirrorValue
        forAllDirections(dir => {
            const value = this.dirs[dir]
            if(value == Direction.LEFT) this.dirs[dir] = Direction.RIGHT
            if(value == Direction.RIGHT) this.dirs[dir] = Direction.LEFT
        })
    }

    get text(): string{
        return ""
    }

    performRenderRotation(graphics: Graphics): void {     
        const ctx = graphics.ctx  
        switch (this.transform.rotValue) {
            case 1:
                ctx.translate(Graphics.TILE_SIZE, 0)
                ctx.rotate(0.5 * Math.PI)    
                break
            case 2:
                ctx.translate(Graphics.TILE_SIZE, Graphics.TILE_SIZE)
                ctx.rotate(Math.PI)
                break
            case 3:
                ctx.translate(0, Graphics.TILE_SIZE)
                ctx.rotate(-0.5 * Math.PI)
                
        }
        if(this.transform.mirrorValue){
            ctx.translate(Graphics.TILE_SIZE, 0)
            ctx.scale(-1, 1)
        }
    }
    renderBottomLayerUnrotated(graphics: Graphics): void{}
    renderTopLayerUnrotated(graphics: Graphics): void { }
    
    renderTopLayer(graphics: Graphics): void{
        const ctx = graphics.ctx
        ctx.save()
        this.performRenderRotation(graphics)
        this.renderTopLayerUnrotated(graphics)
        ctx.restore()
    }

    renderBottomLayer(graphics: Graphics): void {
        const ctx = graphics.ctx
        ctx.save()
        this.performRenderRotation(graphics)
        this.renderBottomLayerUnrotated(graphics)
        ctx.restore()
    }

    renderUILayer(graphics: Graphics): void{
        
    }



    addPowerComponent(type: PowerComponentType, limit: number){
        this.powerComponent = new PowerComponent(this, type, limit)
    }

    canBeBuiltOn(tile: Tile): boolean {
        return false
    }

    isSolid(): boolean{
        return true
    }
    acceptsSource(str: Structure): boolean {
        return false
    }

    acceptsSink(str: Structure): boolean {
        return false
    }

    addSink(str: Structure){}
    addSource(str: Structure){}

    onRemoveNeighbour(nb: Structure): void{

    }

    onAddNeighbour(nb: Structure): void{
        
    }

    renderAdditionalStructInfo(graphics: Graphics): void{

    }

    get team(): boolean{
        return true
    }

    takeDamage(dmg: number): void{
        this.hp -= dmg        
        if(this.hp < 0){
            this.location!.world.removeStructure(this)
            this.location!.world.place(this.location!.pos, new Empty())
        }
    }

    renderStructInfo(graphics: Graphics): void{
        if(this instanceof Empty) return
        let {ctx} = graphics
        ctx.font = "bold " + Graphics.TILE_SIZE / 2 + "px Arial"
        ctx.fillText(this.name, 0, 0)
        ctx.translate(0, Graphics.H_TILE_SIZE )
        ctx.font = Graphics.TILE_SIZE / 3 + "px Arial"
        ctx.fillText(this.text, 0, 0)
        ctx.translate(0, Graphics.H_TILE_SIZE / 3 )
        ctx.scale(2, 2)
        this.renderBottomLayer(graphics)
        ctx.scale(0.5, 0.5)
        ctx.translate(0, 2.5 * Graphics.TILE_SIZE)


        // HP-Bar
        //Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, this.hp / this.maxHP, "red", "HP: " + this.hp + "/" + this.maxHP, true)

        this.renderAdditionalStructInfo(graphics)
    }
}

export class Empty extends Structure{
    constructor(){
        super()
    }

    get name(){
        return "EMPTY"
    }

    isSolid(){
        return false
    }

    onRemoveNeighbour(nb: Structure){}
}