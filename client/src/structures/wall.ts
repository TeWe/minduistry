import { Graphics } from "../graphics/graphics"
import { Cost } from "../resources/cost"
import { ResourceType } from "../resources/resTypes"
import { loadImage } from "../util"
import { Tile } from "../world/tile"
import { Structure } from "./structure"

export class Wall extends Structure{
    constructor(){
        super()

    }

    get name(){
        return "Mauer"
    }

    get text(){
        return "Das einzige, was Alex je gebaut hat."
    }

    get maxHP(){
        return 500
    }



    renderBottomLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx
        ctx.drawImage(img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }
    
    step(){

    }

    get costs(){
        return new Cost([ResourceType.COPPER, 25])
    }

    canBeBuiltOn(tile: Tile): boolean{
        return true
    }
}

const img = loadImage("structures/wall.png")