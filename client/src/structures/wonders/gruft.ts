import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Structure } from "../structure"

export class Gruft extends Structure{
    constructor(){
        super()
    }

    get name(){
        return "Kapuzinergruft"
    }

    get text(){
        return "Erhöht die Geschwindigkeit benachbarter Gruben"
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    step(){

    }

    get costs(){
        return new Cost([ResourceType.SAND, 500])
    }

    canBeBuiltOn(tile: Tile){
        return true
    }



    onRemoveNeighbour(nb: Structure){
        
    }
}

const IMG = loadImage("structures/wonders/gruft.png")