class Bow extends Structure{
    constructor(){
        super()
        

        this.storage = new SingularStorage(10)
        this.radius = 3
        this.shootingCountDown = CountDown.perMinute(13, () => this.shoot(), true)
    }

    get name(){
        return "Bogenschießer"
    }

    renderUILayer(ctx){
        if(!ctx.mouseInTile(this.tile)) return
        ctx.save()
        ctx.beginPath()
        ctx.arc(Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE, this.radius * Graphics.TILE_SIZE, 0, 2 * Math.PI)
        ctx.globalAlpha = 0.1
        ctx.fillStyle = "green"
        ctx.fill()
        ctx.restore()
    }

    renderBottomLayerUnrotated(ctx){
        ctx.save()
        ctx.drawImage(Bow.img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        ctx.restore()
    }

    shoot(){
        if(this.storage.empty) return false
        let targets = this.world.units.filter(unit => !unit.spawning && Vector.distance(unit.pos, this.midPos) < this.radius)
        let target = getRandomListElement(targets)
       
        if(target){
            this.world.bullets.push(new CopperBullet({x: this.midPos.x, y: this.midPos.y}, {x: target.pos.x, y: target.pos.y}))
            this.storage.remove()
            return true
        }
        else return false

        //if(getRandomInt(10) > 8) this.world.bullets.push(new CopperBullet({x: this.midPos.x, y: this.midPos.y}, {x: 0, y: 0}))
    }
    
    step(){
        this.shootingCountDown.step()
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 30])
    }

    canBeBuiltOn(tile){
        return true
    }

    addSource(nb) {
        
    }

    acceptsSource(nb) {
        return this.neighbours.includes(nb)
    }

    canReceive(from, res) {
        return res.resType == ResourceType.COPPER && !this.storage.full
    }

    receive(from, res) {
        this.storage.add()
    }

    renderAdditionalStructInfo(ctx){
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.storage.progress, "orange", "Munition:", true)
        Graphics.renderBar(ctx, 0, 0, 3 * Graphics.TILE_SIZE, Graphics.H_TILE_SIZE, this.shootingCountDown.progress, "orange", "Nachladen:")
    }
    onRemoveNeighbour(nb){}
}

Bow.img = loadImage("structures/towers/bow.png")