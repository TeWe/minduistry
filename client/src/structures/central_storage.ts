import { Graphics } from "../graphics/graphics"
import { Cost } from "../resources/cost"
import { ResourceType } from "../resources/resTypes"
import { Resource } from "../resources/resource"
import { loadImage } from "../util"
import { Tile } from "../world/tile"
import { Structure } from "./structure"

export class CentralStorage extends Structure{
    constructor(){
        super()
    }

    get name(){
        return "Zentrallager"
    }

    get text(){
        return "Resourcen bitte hier hin liefern."
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    step(){

    }

    get costs(): Cost{
        return new Cost([ResourceType.COPPER, 50])
    }

    canReceive(from: Structure, res: Resource){
        return true
    }

    receive(from: Structure, res: Resource): void{
        this.world!.globalStorage.add(res.resType)
    }

    acceptsSource(nb: Structure): boolean{
        return this.neighbours.includes(nb)
    }

    canBeBuiltOn(tile: Tile): boolean{
        return true
    }
}

const img = loadImage("structures/storage.png")