import { Rectangle } from "../geo/geo"
import { Graphics } from "../graphics/graphics"
import { Cost } from "../resources/cost"
import { ResourceType } from "../resources/resTypes"
import { Resource } from "../resources/resource"
import { getRandomListElement, loadImage } from "../util"
import { Tile } from "../world/tile"
import { World } from "../world/world"
import { SiliconPress } from "./factories/silicon_press"
import { Furnace } from "./furnace"
import { Joiner } from "./pipes/joiner"
import { Shovel } from "./mines/shovel"
import { PowerNode } from "./power/power_node"
import { Splitter } from "./pipes/splitter"
import { Structure } from "./structure"
import { Gruft } from "./wonders/gruft"
import { Garden } from "./mines/garden"
import { ElectricMine } from "./mines/electric_mine"
import { IronPickaxe } from "./mines/iron_pickaxe"
import { CentralStorage } from "./central_storage"

export class Laboratory extends Structure{

    activeResearch?: TechTreeNode

    constructor(){
        super()
    }

    get name(){
        return "Labor"
    }

    get text(){
        return "Für Forschung"
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    step(){

    }

    get costs(){
        return new Cost([ResourceType.COPPER, 15])
    }

    canReceive(from: Structure, res: Resource): boolean{
        if(!this.activeResearch) return false
        return this.activeResearch!.cost.accepts(res)
    }

    receive(from: Structure, res: Resource){
        const cost = this.activeResearch!.cost
        cost.pay(res.resType, 1)
        if(cost.isPaid()){
            Laboratory.research(this.location!.world, this.activeResearch!)
            this.activeResearch = undefined
        }
    }

    acceptsSource(nb: Structure){
       return true
    }

    canBeBuiltOn(tile: Tile){
        return true
    }

    addSource(nb: Structure){
        
    }

    onRemoveNeighbour(nb: Structure){
        
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const {ctx, inputHandler, context} = graphics
        if(inputHandler.mouseInRect(new Rectangle(0, 0, 100, 100))){
            ctx.font = "20pt Arial"
        }
        let h = Graphics.TILE_SIZE * 0.5
        let tmpy = 0
        for(let t of Laboratory.researchableNodes()){
            const rect = new Rectangle(-5, -15, Graphics.tsize(4), Graphics.tsize(1.1))
            if(context.mouseInRect(rect, true)){
                ctx.beginPath()
                context.rect(rect)
                ctx.stroke()
            }
            context.onClickInRect(rect, () => {
                this.activeResearch = t
                console.log("uhuh")
            }, true)

            if(this.activeResearch == t){
                ctx.beginPath()
                ctx.lineWidth = 3
                context.rect(rect)
                ctx.stroke()
            }
            Graphics.renderResourceSet(ctx, t.cost, {x: 0, y: tmpy}, Graphics.TILE_SIZE * 0.5, t.name, true)
        }

        

    }

    private static research(gw: World, tech: TechTreeNode){
        let menu = gw.menu!
    
        tech.researched = true
        Laboratory.updateAvailableResearch()
    
        // unlock new buildings
        menu.forAllMenuItems(item => {
            for(let cl of tech.unlocks) if(item.renderStr instanceof cl) item.unlocked = true
        })
    }

    static updateAvailableResearch(){
        const a = Object.values(techtree).filter(tech => {
            if(tech.researched) return false
            return tech.precond.map(id => techtree[id]).every(t => t.researched)
        })
        availableResearch = a
        
    }

    static researchableNodes(): TechTreeNode[]{
        return availableResearch
    }
}


const techtree: Record<string, TechTreeNode> = {
    ["INIT"]: {
        name: "Grundlagenforschung",
        descr: "Schaltet keine neuen Gebäude frei.",
        precond: [],
        unlocks: [],
        cost: new Cost([ResourceType.COPPER, 10]),
        researched: false
    },
    ["DIG1"]: {
        name: "Grabtechnologie I",
        descr: "Schaltet Schaufel frei.",
        precond: ["INIT"],
        unlocks: [Shovel],
        cost: new Cost([ResourceType.COPPER, 50]),
        researched: false
    },
    ["POWER1"]: {
        name: "Strom I",
        descr: "Schaltet Strommastens frei.",
        precond: ["INIT"],
        unlocks: [PowerNode],
        cost: new Cost([ResourceType.COPPER, 50], [ResourceType.COAL, 15]),
        researched: false,
    },
    ["BURN1"]: {
        name: "Kohleverbrennung",
        descr: "Schaltet Ofen frei.",
        precond: ["INIT", "POWER1"],
        unlocks: [Furnace],
        cost: new Cost([ResourceType.COPPER, 20], [ResourceType.COAL, 15]),
        researched: false
    },
    ["DIG2"]: {
        name: "Grabtechnologie II",
        descr: "Schaltet Schaufel frei.",
        precond: ["DIG1"],
        unlocks: [Gruft],
        cost: new Cost([ResourceType.COPPER, 50], [ResourceType.SAND, 200]),
        researched: false
    },
    ["SILICON"]: {
        name: "Silikon",
        descr: "Schaltet die Silikonpresse frei.",
        precond: ["DIG1", "POWER1"],
        unlocks: [SiliconPress],
        cost: new Cost([ResourceType.COPPER, 50], [ResourceType.SAND, 200]),
        researched: false
    },
    ["LOGISTICS2"]: {
        name: "Logistik II",
        descr: "Schaltet Splitter und Joiner frei.",
        precond: ["SILICON"],
        unlocks: [Splitter, Joiner],
        cost: new Cost([ResourceType.SILICON, 20], [ResourceType.COPPER, 40]),
        researched: false
    },
    ["GARDENING1"]: {
        name: "Gemüsetechnoligie I",
        descr: "Schaltet Gärten frei",
        precond: ["INIT"],
        unlocks: [Garden],
        cost: new Cost([ResourceType.COPPER, 50]),
        researched: false
    },
    ["LOGISTICS1"]: {
        name: "Logistik 1",
        descr: "Schaltet Zentrallager zum Resourcen abladen frei",
        precond: ["INIT"],
        unlocks: [CentralStorage],
        cost: new Cost([ResourceType.COPPER, 20]),
        researched: false
    },
    ["EMINING"]: {
        name: "Elektro-Mine",
        descr: "Schaltet elektrische Minen",
        precond: ["POWER1"],
        unlocks: [IronPickaxe],
        cost: new Cost([ResourceType.COPPER, 50]),
        researched: false
    }
} satisfies Record<string, TechTreeNode>


export type TechTreeNode = {
    name: string
    descr: string,
    precond: string[],
    unlocks: (typeof Structure)[],
    cost: Cost,
    researched: boolean
}


let availableResearch: TechTreeNode[] = [techtree["INIT"]]
Laboratory.updateAvailableResearch()


const IMG = loadImage("structures/science.png")