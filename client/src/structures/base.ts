import { Graphics } from "../graphics/graphics"
import { Cost } from "../resources/cost"
import { ResourceType } from "../resources/resTypes"
import { Resource } from "../resources/resource"
import { loadImage } from "../util"
import { Tile } from "../world/tile"
import { CentralStorage } from "./central_storage"
import { Structure } from "./structure"

export class Base extends CentralStorage{
    constructor(){
        super()
    }

    get name(){
        return "Inselbasis"
    }

    get text(){
        return "Resourcen bitte hier hin liefern."
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    step(){

    }

    get costs(): Cost{
        return new Cost([ResourceType.COPPER, 50])
    }

    canReceive(from: Structure, res: Resource){
        return true
    }

    receive(from: Structure, res: Resource){
        this.world!.globalStorage.add(res.resType)
    }

    acceptsSource(nb: Structure): boolean{
        return this.neighbours.includes(nb)
    }

    canBeBuiltOn(tile: Tile): boolean{
        return true
    }

    addSource(nb: Structure){
        
    }

    onRemoveNeighbour(nb: Structure){
        
    }
}

const img = loadImage("structures/base.png")