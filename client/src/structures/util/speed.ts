export class Speed{
    perMilli: number

    private constructor(perMilli: number){
        this.perMilli = perMilli
    }

    static unitsPerMilli(perMilli: number): Speed{
        return new Speed(perMilli)
    }

    static unitsPerSecond(perSecond: number): Speed{
        return new Speed(perSecond * 0.001)
    }

    public applyToMilliseconds(delta: number): number{
        return this.perMilli * delta
    }

    public applyToSeconds(delta: number): number{
        return this.perMilli * delta
    }

    public applyFactor(f: number){
        this.perMilli *= f
    }
}