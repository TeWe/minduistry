export class CountDown {
  private totalMillis: number
  private millisPassed: number
  private rep: boolean 
  onCompletion: () => boolean
  
  private constructor(totalMillis: number, onCompletion: () => boolean = () => {return true}, repeating = false) {
      this.totalMillis = totalMillis
      this.millisPassed = 0
      this.onCompletion = onCompletion
      this.rep = repeating
    }
  
    step(delta: number): void {
      if (this.millisPassed <= this.totalMillis){
        this.millisPassed += delta
        if(this.millisPassed >= this.totalMillis) {
          if(this.onCompletion()) {
            if(this.rep) this.reset()
          }
          else{
            this.millisPassed = this.totalMillis - 1
          }
        }
      }
    }
  
    get progress(): number {
      return this.millisPassed / this.totalMillis
    }
  
    get completed(): boolean {
      return this.millisPassed >= this.totalMillis
    }
  
    reset(): void {
      this.millisPassed = 0
    }

    static timesPerMinute(amount: number, f: () => boolean, rep: boolean = true){
        return new CountDown(60000 / amount, f, rep)
    }

    static seconds(numberOfSeconds: number, f: () => boolean, rep: boolean = false): CountDown{
      return new CountDown(numberOfSeconds, f, rep)
    }

    static dummy(): CountDown{
      return new CountDown(10000, () => false, false)
    }
  }