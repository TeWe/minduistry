import { Resource } from "../../resources/resource"
import { Speed } from "./speed"

export type ProgressingResource = {
    res: Resource,
    progress: number
}

export class ResourceQueue{
    private capacity: number
    private bandWidth: number
    private content: ProgressingResource[]
    private youngest?: ProgressingResource

    private speed: Speed

    private onResourceMove?: (res: ProgressingResource, prog: number) => void

    constructor(capacity: number, speed: Speed, onResourceMove?:  (res: ProgressingResource, prog: number) => void){
        this.capacity = capacity
        this.bandWidth = 1.0 / this.capacity
        this.content = []
        this.youngest = undefined
        this.onResourceMove = onResourceMove
        this.speed = speed
    }

    isFree(): boolean{
        if(this.content.length == 0) return true
        return this.youngest!.progress > this.bandWidth
    }

    step(delta: number): void{
        const deltaProgress = this.speed.applyToMilliseconds(delta)
        let last = 1 + this.bandWidth
        for(let i = 0; i < this.content.length; i++){
            let res = this.content[i]
            const possibleProgress = Math.min(deltaProgress, last - this.bandWidth - res.progress)
            res.progress += possibleProgress
            if(this.onResourceMove) this.onResourceMove(res, possibleProgress)
            last = res.progress
        }
    }

    input(res: Resource){
        const progRes = {res: res, progress: 0}
        this.youngest = progRes
        this.content.push(progRes)
    }

    forEach(f: (res: Resource, progress: number) => void): void{
        this.content.forEach(progRes => f(progRes.res, progRes.progress))
    }

    getPotentialOutput(): Resource | undefined{
        if(this.content.length == 0) return undefined
        let res = this.content[0]
        if(res.progress >= 1) return res.res
        return undefined
    }

    remove(out: Resource){
        this.content = this.content.filter(res => res.res != out)
    }
}