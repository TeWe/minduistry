class Smoothener{
    constructor(factor, val = 0){
        this.f = factor
        this.val = val
        this.inVal = 1 - factor
    }

    record(v){
        this.val = v
        return
        this.val = this.f * this.val + this.inVal * v
    }
}