import { CountDown } from "./countdown"

export class Averager{
    private nturns: number
    private records: number[]
    private i: number
    public val: number
    private internalVal: number
    private updateCountDown: CountDown



    constructor(nturns: number, val: number = 0, updatePerMinute: number = 60){
        this.records = []
        this.nturns = nturns
        for(let i=0; i<nturns; i++) this.records.push(val)
        this.i = 0
        this.val = val
        this.internalVal = val
    
        this.updateCountDown = CountDown.timesPerMinute(updatePerMinute, () => {this.val = this.internalVal; return true})
    }

    record(v: number): void{
        this.val += (v -  this.records[this.i]) / this.nturns
        this.records[this.i] = v
        this.i = (this.i + 1) % this.nturns
    }
}