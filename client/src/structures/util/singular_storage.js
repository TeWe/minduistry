class SingularStorage{
    constructor(capacity){
        this.capacity = capacity
        this.stored = 0
    }

    add(am = 1){
        this.stored = Math.min(this.stored + am, this.capacity)
    }

    remove(am = 1){
        this.stored = Math.max(this.stored - am, 0)
    }

    get full(){
        return this.stored == this.capacity
    }

    get progress(){
        return this.stored / this.capacity
    }

    get empty(){
        return this.stored == 0
    }


}