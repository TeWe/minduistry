import { Graphics } from "../../graphics/graphics"
import { round } from "../../util"
import { CountDown } from "./countdown"

export class PerMinuteCounter{
    private windows: PMC_Window[]
    private oldest: number
    private value: number
    private outputCountDown: CountDown

    constructor(){
        this.windows = [new PMC_Window()]
        this.oldest = 0
        this.value = 0
        this.outputCountDown = CountDown.timesPerMinute(60, () => {
            this.updateValue()
            return true
        })
    }

    step(delta: number): void{
        let l = this.windows.length
        if(l < 5 && this.windows[l - 1].age >= Graphics.FPM){
            this.windows.push(new PMC_Window())
        }

        this.windows.forEach(w => w.step())
        this.outputCountDown.step(delta)
    }

    register(am = 1){
        this.windows.forEach(w => w.register(am))
    }

    updateValue(): void{
        let next = (this.oldest + 1) % this.windows.length
        if(this.windows[next].age > this.windows[this.oldest].age) this.oldest = next;
        this.value = round(this.windows[this.oldest].query())
    }

    query(){
        return this.value
    }
}

class PMC_Window{
    public age: number
    private val: number

    constructor(){
        this.age = 0
        this.val = 0
    }

    register(am: number = 1): void{
        this.val += am
    }

    step(): void{
        this.age++
        if(this.age > PMC_Window.LENGTH){
            this.age = 0
            this.val = 0
        }
    }

    query(bonus: number = 0): number{
        return (this.val + bonus) / this.age * Graphics.FPM
    }

    static get LENGTH(): number{
        return 5*Graphics.FPM;
    }
    
}