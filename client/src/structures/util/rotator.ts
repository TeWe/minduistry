export class Rotator<T>{
    private n: number
    private i: number
    private list: T[]

    constructor(list: T[]){
        this.list = list
        this.n = list.length
        this.i = 0
    }

    doForNext(f: (t: T) => boolean): boolean{
        let j = this.i
        let done = false
        do{
            if(f(this.list[j])) {
                done = true
                break
            }
            j = (j + 1) % this.n
        } while(j != this.i)
        this.i = (j + 1) % this.n
        return done
    }
}