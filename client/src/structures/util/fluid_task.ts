import { Graphics } from "../../graphics/graphics"

export class FluidTask {

  perTick: () => void

    constructor(perMinute: number, perTick = (_: number) => {}) {
      let perStep = perMinute / Graphics.FPM
      this.perTick = () => perTick(perStep)
    }
  
    step() {
      this.perTick()
    }
}