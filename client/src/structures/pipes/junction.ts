import { Direction } from "../../geo/direction"
import { interpolate } from "../../geo/geo"
import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Structure } from "../structure"
import { ResourceQueue } from "../util/res_queue"
import { getBasicPipeSpeed } from "./pipe"

export class Junction extends Structure {
    
    queue1: ResourceQueue
    queue2: ResourceQueue

    inputStructure1?: Structure
    inputStructure2?: Structure

    outputStructure1?: Structure
    outputStructure2?: Structure

    constructor() {
        super()
        
        this.queue1 = new ResourceQueue(3, getBasicPipeSpeed())
        this.queue2 = new ResourceQueue(3, getBasicPipeSpeed())
    }

    private get inputDir1(): Direction{
        return this.dirs[Direction.UP]
    }
    private get inputDir2(): Direction{
        return this.dirs[Direction.RIGHT]
    }
    private get outputDir1(): Direction{
        return this.dirs[Direction.DOWN]
    }
    private get outputDir2(): Direction{
        return this.dirs[Direction.LEFT]
    }

    renderTopLayerUnrotated(graphics: Graphics){
        // clip the up-down pipe, since it goes underneath the horizontal one
        const ctx = graphics.ctx
        ctx.save()
        ctx.beginPath()
        ctx.rect(0, -Graphics.TILE_SIZE, Graphics.TILE_SIZE, 1.25 * Graphics.TILE_SIZE)
        ctx.rect(0, 0.75 * Graphics.TILE_SIZE, Graphics.TILE_SIZE, 1.25 * Graphics.TILE_SIZE)
        ctx.clip()
        
        this.queue1.forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate({ x: 0.5 * Graphics.TILE_SIZE, y: 0 }, { x: 0.5 * Graphics.TILE_SIZE, y: 1 * Graphics.TILE_SIZE }, progress), this.transform)
        })

        ctx.restore()
        this.queue2.forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate({ x: 1 * Graphics.TILE_SIZE, y: 0.5 * Graphics.TILE_SIZE }, { x: 0, y: 0.5 * Graphics.TILE_SIZE }, progress), this.transform)
        })
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name() {
        return "Kreuzung"
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 5])
    }

    canBeBuiltOn(tile: Tile): boolean {
        return true
    }

    addSource(nb: Structure) {
        if(nb == this.getNeighbour(this.inputDir1))
            this.inputStructure1 = nb
        else
            this.inputStructure2 = nb
    }

    addSink(nb: Structure) {
        if(nb == this.getNeighbour(this.outputDir1))
            this.outputStructure1 = nb
        else
            this.outputStructure2 = nb
    }

    acceptsSource(nb: Structure): boolean {
        return nb == this.getNeighbour(this.inputDir1) || nb == this.getNeighbour(this.inputDir2)
    }

    acceptsSink(nb: Structure): boolean {
        return nb == this.getNeighbour(this.outputDir1) || nb == this.getNeighbour(this.outputDir2)
    }

    onRemoveNeighbour(nb: Structure){
        if(nb == this.outputStructure1) this.outputStructure1 = undefined
        if(nb == this.inputStructure1) this.inputStructure1 = undefined
        if(nb == this.outputStructure2) this.outputStructure2 = undefined
        if(nb == this.inputStructure2) this.inputStructure2 = undefined   
    }

    canReceive(from: Structure, res: Resource): boolean {
        if (!from) return false
        if(from == this.inputStructure1)
            return this.queue1.isFree()
        if(from == this.inputStructure2)
            return this.queue2.isFree()
        return false
    }

    receive(from: Structure, res: Resource) {
        if(from == this.inputStructure1)
            this.queue1.input(res)
        else
            this.queue2.input(res)
    }

    step(delta: number) {
        this.queue1.step(delta)
        let output1 = this.queue1.getPotentialOutput()
        if (output1 && this.outputStructure1 && this.outputStructure1.canReceive(this, output1)) {
            this.queue1.remove(output1)
            this.outputStructure1.receive(this, output1)
        }

        this.queue2.step(delta)
        let output2 = this.queue2.getPotentialOutput()
        if (output2 && this.outputStructure2 && this.outputStructure2.canReceive(this, output2)) {
            this.queue2.remove(output2)
            this.outputStructure2.receive(this, output2)
        }
    }
}

const IMG = loadImage("structures/junction.png")