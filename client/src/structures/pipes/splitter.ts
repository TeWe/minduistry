import { Direction } from "../../geo/direction"
import { interpolate } from "../../geo/geo"
import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Pipe, getBasicPipeSpeed } from "./pipe"

import { Structure } from "../structure"
import { ResourceQueue } from "../util/res_queue"
import { Rotator } from "../util/rotator"
import { Speed } from "../util/speed"


function getBasicSplitterPartSpeed(): Speed{
    const speed = getBasicPipeSpeed()
    speed.applyFactor(1)
    return speed
}

export class Splitter extends Structure{
    private outputQueues: [ResourceQueue, ResourceQueue, ResourceQueue]
    private inputQueue : ResourceQueue

    private outputStructures: [Structure | null, Structure | null, Structure | null]
    private inputStructure: Structure | null

    rotator: Rotator<ResourceQueue>

    constructor() {
        super()

        
        this.outputQueues = [new ResourceQueue(2, getBasicSplitterPartSpeed()), new ResourceQueue(2, getBasicSplitterPartSpeed()), new ResourceQueue(2, getBasicSplitterPartSpeed())]
        this.outputStructures = [null, null, null]

        this.inputQueue = new ResourceQueue(2, getBasicSplitterPartSpeed())
        this.inputStructure = null

        this.rotator = new Rotator(this.outputQueues)
    }

    private get outputDir1(): Direction{
        return this.dirs[Direction.LEFT]
    }
    private get outputDir2(): Direction{
        return this.dirs[Direction.RIGHT]
    }
    private get outputDir3(): Direction{
        return this.dirs[Direction.DOWN]
    }
    private get inputDir(): Direction{
        return this.dirs[Direction.UP]
    }

    renderTopLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx      
        let mid = { x: 0.5 * Graphics.TILE_SIZE, y: 0.5 * Graphics.TILE_SIZE}
        this.outputQueues[0].forEach((res, prog) => {
            res.render(
                ctx, 
                interpolate(mid, { x: 0, y: 0.5 * Graphics.TILE_SIZE}, prog), this.transform)
        })

        this.outputQueues[1].forEach((res, prog) => {
            res.render(
                ctx, 
                interpolate(mid, { x: 1 * Graphics.TILE_SIZE, y: 0.5 * Graphics.TILE_SIZE }, prog), this.transform)
        })

        this.outputQueues[2].forEach((res, prog) => {
            res.render(
                ctx, 
                interpolate(mid, { x: 0.5 * Graphics.TILE_SIZE, y: 1 * Graphics.TILE_SIZE }, prog), this.transform)
        })

        this.inputQueue.forEach((res, prog) => {
            res.render(
                ctx, 
                interpolate({x: 0.5 * Graphics.TILE_SIZE, y: 0}, mid, prog), this.transform)
        })
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(img, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name() {
        return "Splitter"
    }

    get text(){
        return "Verteilt Items regelmäßig in 3 Richtungen."
    }

    canBeBuiltOn(tile: Tile): boolean {
        return true
    }

    addSource(nb: Structure): void{
        this.inputStructure = nb
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 5], [ResourceType.COAL, 2])
    }

    addSink(nb: Structure) {   
        if(nb == this.getNeighbour(this.outputDir1!))
            this.outputStructures[0] = nb
        else if(nb == this.getNeighbour(this.outputDir2!))
            this.outputStructures[1] = nb
        else this.outputStructures[2] = nb
    }

    acceptsSink(nb: Structure) {
        return nb == this.getNeighbour(this.outputDir1) || nb == this.getNeighbour(this.outputDir2) || nb == this.getNeighbour(this.outputDir3)
    }

    acceptsSource(nb: Structure) {
        return nb == this.getNeighbour(this.inputDir)
    }

    canReceive(from: Structure, res: Resource): boolean {
        if(!from) return false
        return this.inputQueue.isFree()
    }

    receive(from: Structure, res: Resource): void {
        this.inputQueue.input(res)
    }

    step(delta: number): void{
        for(let q of this.outputQueues) q.step(delta)
        this.inputQueue.step(delta)

        // handle movement from input to output queues
        const potentialOutput = this.inputQueue.getPotentialOutput()
        if(potentialOutput){
            this.rotator.doForNext(outputQueue => {
                if(!outputQueue.isFree()) return false
                this.inputQueue.remove(potentialOutput)
                outputQueue.input(potentialOutput)
                return true
            })
        }

        // handle output of outputQueues
        for(let i = 0; i<3; i++){
            let output = this.outputQueues[i].getPotentialOutput()
            if(!output || !this.outputStructures[i] || !this.outputStructures[i]!.canReceive(this, output)) continue
            this.outputStructures[i]!.receive(this, output)   
            this.outputQueues[i].remove(output)
        }
    }

    onRemoveNeighbour(nb: Structure){
        if(nb == this.outputStructures[0]) this.outputStructures[0] = null
        if(nb == this.outputStructures[1]) this.outputStructures[1] = null
        if(nb == this.outputStructures[2]) this.outputStructures[2] = null
        if(nb == this.inputStructure) this.inputStructure = null
    }
}

const img = loadImage("structures/splitter.png")