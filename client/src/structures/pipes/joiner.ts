import { Direction } from "../../geo/direction"
import { Structure } from "../structure"
import { getBasicPipeSpeed } from "./pipe"
import { ResourceQueue } from "../util/res_queue"
import { Rotator } from "../util/rotator"
import { Graphics } from "../../graphics/graphics"
import { interpolate } from "../../geo/geo"
import { Tile } from "../../world/tile"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { Resource } from "../../resources/resource"
import { loadImage } from "../../util"

function getBasicJoinerSpeed(){
    const speed = getBasicPipeSpeed()
    speed.applyFactor(1)
    return speed
}

export class Joiner extends Structure{

    inputQueues: [ResourceQueue, ResourceQueue, ResourceQueue]
    outputQueue: ResourceQueue
    rotator: Rotator<ResourceQueue>

    inputStructure1?: Structure
    inputStructure2?: Structure
    inputStructure3?: Structure
    outputStructure?: Structure

    constructor() {
        super()



        this.inputQueues = [new ResourceQueue(2, getBasicJoinerSpeed()), new ResourceQueue(2, getBasicJoinerSpeed()), new ResourceQueue(2, getBasicJoinerSpeed())]
        this.outputQueue = new ResourceQueue(2, getBasicJoinerSpeed()) // Outputqueue

        this.rotator = new Rotator(this.inputQueues)
    }

    get inputDir1(): Direction{
        return this.dirs[Direction.LEFT]
    }
    get inputDir2(): Direction{
        return this.dirs[Direction.RIGHT]
    }
    get inputDir3(): Direction{
        return this.dirs[Direction.DOWN]
    }
    get outputDir(): Direction{
        return this.dirs[Direction.UP]
    }

    renderTopLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx      
        let mid = { x: 0.5 * Graphics.TILE_SIZE, y: 0.5 * Graphics.TILE_SIZE}
        this.inputQueues[0].forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate({ x: 0, y: 0.5 * Graphics.TILE_SIZE}, mid, progress), this.transform)
        })

        this.inputQueues[1].forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate({ x: 1 * Graphics.TILE_SIZE, y: 0.5 * Graphics.TILE_SIZE }, mid, progress), this.transform)
        })

        this.inputQueues[2].forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate({ x: 0.5 * Graphics.TILE_SIZE, y: 1 * Graphics.TILE_SIZE }, mid, progress), this.transform)
        })

        this.outputQueue.forEach((res, progress) => {
            res.render(
                ctx, 
                interpolate(mid, {x: 0.5 * Graphics.TILE_SIZE, y: 0}, progress), this.transform)
        })
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
    }

    get name() {
        return "Joiner"
    }

    get text(){
        return "Führt 3 Eingänge zu einem Ausgang."
    }

    canBeBuiltOn(tile: Tile) {
        return true
    }

    addSource(nb: Structure) {
        if(nb == this.getNeighbour(this.inputDir1))
            this.inputStructure1 = nb
        else if(nb == this.getNeighbour(this.inputDir2))
            this.inputStructure2 = nb
        else this.inputStructure3 = nb
    }

    get costs(){
        return new Cost([ResourceType.COPPER, 5], [ResourceType.COAL, 2])
    }

    addSink(nb: Structure) {   
        this.outputStructure = nb
    }

    acceptsSource(nb: Structure) {
        return nb == this.getNeighbour(this.inputDir1)
            || nb == this.getNeighbour(this.inputDir2)
            || nb == this.getNeighbour(this.inputDir3)
    }

    acceptsSink(nb: Structure) {
        return nb == this.getNeighbour(this.outputDir)
    }

    canReceive(from: Structure, res: Resource) {
        if (!from) return false
        if(from == this.inputStructure1)
            return this.inputQueues[0].isFree()
        if(from == this.inputStructure2)
            return this.inputQueues[1].isFree()
        if(from == this.inputStructure3)
            return this.inputQueues[2].isFree()
        return false
    }

    receive(from: Structure, res: Resource) {
        if(from == this.inputStructure1)
            this.inputQueues[0].input(res)
        else if(from == this.inputStructure2)
            this.inputQueues[1].input(res)
        else
            this.inputQueues[2].input(res)
    }

    step(delta: number) {
        for(let q of this.inputQueues) q.step(delta)
        this.outputQueue.step(delta)

        // handle joiner output
        let output = this.outputQueue.getPotentialOutput()
        if (output && this.outputStructure && this.outputStructure.canReceive(this, output)) {
            this.outputQueue.remove(output)
            this.outputStructure.receive(this, output)
        }

        // handle movement from input to output queues
        if(this.outputQueue.isFree()){
            this.rotator.doForNext(inputQueue => {
                let potentialOutput = inputQueue.getPotentialOutput()
                if(!potentialOutput) return false
                inputQueue.remove(potentialOutput)
                this.outputQueue.input(potentialOutput)
                return true
            })
        }
    }

    onRemoveNeighbour(nb: Structure){
        if(nb == this.outputStructure) this.outputStructure = undefined
    }
}

const IMG = loadImage("structures/router.png")