import { interpolate } from "../../geo/geo"
import { Direction } from "../../geo/direction"
import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { Resource } from "../../resources/resource"
import { loadImage } from "../../util"
import { Structure } from "../structure"
import { ProgressingResource, ResourceQueue } from "../util/res_queue"
import { Tile } from "../../world/tile"
import { ResourceType } from "../../resources/resTypes"
import { Speed } from "../util/speed"

type PipeType = "curved" | "straight"


export function getBasicPipeSpeed(): Speed{
    return Speed.unitsPerSecond(1)
}

const straightImg = loadImage("structures/pipe_straight.png")
const curvedImg = loadImage("structures/pipe_curve.png")

export class Pipe extends Structure {
    
    type: PipeType
    queue: ResourceQueue

    outputStructure?: Structure
    inputStructure?: Structure

    constructor(type: PipeType) {
        super()
        this.type = type


        let rotatorFunction: (res: ProgressingResource, prog: number) => void
        if(this.type == "straight") rotatorFunction = () => {}
        else rotatorFunction = (res, prog) => {
            res.res.rotateBy(0.5 * Math.PI * prog * (this.transform.mirrorValue ? -1 : 1))
        }

        this.queue = new ResourceQueue(3, getBasicPipeSpeed(), rotatorFunction)
    }

    private get inputDir(): Direction{
        if(this.type == "straight")
            return this.dirs[Direction.UP]
        else return this.dirs[Direction.LEFT]
    }
    private get outputDir(): Direction{
        return this.dirs[Direction.DOWN]
    }

    renderTopLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx
        if(this.type == "straight"){
            this.queue.forEach((res, progress) => {
                res.render(
                    ctx, 
                    interpolate({ x: 0.5 * Graphics.TILE_SIZE, y: 0 }, { x: 0.5 * Graphics.TILE_SIZE, y: 1 * Graphics.TILE_SIZE }, progress), this.transform)
            })
        } else{
            ctx.save()
            ctx.translate(0, Graphics.TILE_SIZE)
            this.queue.forEach((res, progress) => {
                res.render(ctx, {
                    x: 0.5 * Graphics.TILE_SIZE * Math.sin(Math.PI * 0.5 * progress),
                    y: -0.5 * Graphics.TILE_SIZE * Math.cos(Math.PI * 0.5 * progress)
                }, this.transform)
            })
            ctx.restore()
        }
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx
        if(this.type == "straight"){
            ctx.drawImage(straightImg, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        } else{
            ctx.drawImage(curvedImg, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        }
    }

    get name(): string{
        return "Fließband"
    }

    get text(): string{
        return "Transportiert Resourcen"
    }

    canBeBuiltOn(tile: Tile): boolean {
        return true
    }

    acceptsSource(nb: Structure) {
        return nb == this.getNeighbour(this.inputDir!)
    }

    acceptsSink(nb: Structure) {
        return nb == this.getNeighbour(this.outputDir!)
    }

    onRemoveNeighbour(nb: Structure): void{
        if(nb == this.outputStructure) this.outputStructure = undefined
        if(nb == this.inputStructure) this.inputStructure = undefined
    }

    addSource(str: Structure): void{
        this.inputStructure = str
    }

    addSink(nb: Structure): void {
        this.outputStructure = nb
    }

    canReceive(from: Structure, res: Resource): boolean {
        if (!this.inputStructure) return false
        if (from != this.inputStructure) return false
        return this.queue.isFree()
    }

    receive(from: Structure, res: Resource): void {
        this.queue.input(res)
    }

    step(delta: number): void {
        this.queue.step(delta)
        let output = this.queue.getPotentialOutput()
        if (output && this.outputStructure && this.outputStructure.canReceive(this, output)) {
            this.queue.remove(output)
            this.outputStructure.receive(this, output)
        }
    }

    get costs(): Cost{
        return new Cost([ResourceType.COPPER, 2])
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        Graphics.renderInfoText(ctx, {x: 0, y: 0}, Graphics.TILE_SIZE, "Transportiert Zeug ")

    }
}




