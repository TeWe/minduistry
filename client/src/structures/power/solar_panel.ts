import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Structure } from "../structure"
import { FluidTask } from "../util/fluid_task"
import { PowerComponentType } from "./power_component"

export class SolarPanel extends Structure{

    task: FluidTask

    constructor(){
        super()
        this.addPowerComponent(PowerComponentType.SOURCE, 5)
        this.task = new FluidTask(5, am => this.powerComponent!.produce(am))
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        graphics.ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        return
    }

    get name(){
        return "Solar Panel"
    }

    get costs(){
        return new Cost()
    }

    step(){
        this.task.step()
    }

    canBeBuiltOn(tile: Tile){
        return true
    }

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        let network = this.powerComponent!.network
        if(!network) {
            Graphics.renderInfoText(ctx, {x: 0, y: 0}, 0.5 * Graphics.TILE_SIZE, "Momentan an kein Stromnetz angeschlossen", true)
            return
        }
        Graphics.renderInfoText(ctx, {x: 0, y: 0}, 0.5 * Graphics.TILE_SIZE, "Max. Prod.: " + PROD_AMOUNT + " Watt", true)
    }
}

const IMG = loadImage("structures/power/solar_panel.png")

const PROD_AMOUNT = 5