import { Structure } from "../structure"
import { PowerNetwork } from "./power_network"

export class PowerComponent{

    storageCapacity: number
    produced: number
    demand: number
    stored: number
    str: Structure
    type: PowerComponentType
    adj: PowerComponent[]
    providesPower: boolean

    network?: PowerNetwork

    constructor(str: Structure, type: PowerComponentType, limit: number){
        this.storageCapacity = limit
        this.produced = 0
        this.demand = 0
        this.stored = 0
        this.str = str
        this.type = type
        this.adj = []
        this.providesPower = false
    }

    produce(am: number): void{
        this.produced = am
    }

    setDemand(dem: number): void{
        this.demand = dem
    }

    fillStorageBy(am: number): void{
        this.stored = Math.min(this.storageCapacity, this.stored + am)
    }

    drainStorageBy(am: number): boolean{
        if(this.stored >= am) {
            this.stored -= am
            return true
        } else return false
    }

    tryToDrainStorageBy(am: number): boolean{
        if(this.stored < am) return false
        this.stored -= am
        return true
    }

    connect(cmp: PowerComponent): void{
        this.adj.push(cmp)
        cmp.adj.push(this)
    }

    disconnect(cmp: PowerComponent): void{
        this.adj = this.adj.filter(x => x != cmp)
        cmp.adj = cmp.adj.filter(x => x != this)
    }

    get availableStorageCapacity(): number{
        return this.storageCapacity - this.stored
    }
}

export const enum PowerComponentType{
    SOURCE = "SOURCE",
    SINK = "SINK",
    TRANSFER = "TRANSFER"
}

