import { Graphics } from "../../graphics/graphics"
import { Cost } from "../../resources/cost"
import { ResourceType } from "../../resources/resTypes"
import { loadImage } from "../../util"
import { Tile } from "../../world/tile"
import { Structure } from "../structure"
import { PowerComponentType } from "./power_component"

export class PowerNode extends Structure{
    constructor(){
        super()
        this.addPowerComponent(PowerComponentType.TRANSFER, 50)
    }

    get radius(){
        return 5
    }

    onPlacement(){
        /*
        this.world.forAllStructures(str => {
            if(str.powerComponent) this.powerComponent.connect(str.powerComponent)
        })
        PowerNetwork.recomputeNetworks(this.world)
        */
    }

    // overwrite rotation function
    rotate(deltaY: number) {
        // do noting
    }

    get costs(): Cost{
        return new Cost([ResourceType.COPPER, 15])
    }

    canBeBuiltOn(tile: Tile){
        return true
    }

    get name(){
        return "Stromknoten"
    }

    get text(){
        return "Verteilt Strom (mit 'C' die Strom-UI toggeln)"
    }

    renderUILayer(graphics: Graphics){
        if(!graphics.showPower) return
        const {ctx} = graphics
        ctx.save()
        ctx.strokeStyle = "yellow"
        ctx.translate(Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE)
        for(let cmp of this.powerComponent!.adj){
            ctx.beginPath()
            ctx.moveTo(0, 0)
            const ownPos = this.location!.pos
            const targetPos = cmp.str.location!.pos
            let v = {j: targetPos.j - ownPos.j, i: targetPos.i - ownPos.i}
            ctx.lineTo(Graphics.tsize(v.j), Graphics.tsize(v.i))
            ctx.stroke()
        }

        if(graphics.lockedTile == this.location!.tile){
            ctx.beginPath()
            ctx.fillStyle = "yellow"
            ctx.globalAlpha = 0.2
            ctx.arc(Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE, Graphics.tsize(this.radius), 0, 2*Math.PI)
            ctx.fill()

        }
        ctx.restore()
    }

    renderBottomLayerUnrotated(graphics: Graphics){
        const ctx = graphics.ctx
        ctx.drawImage(IMG, 0, 0, Graphics.TILE_SIZE, Graphics.TILE_SIZE)
        return
        
        ctx.save()
        ctx.beginPath()
        ctx.arc(Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE, Graphics.H_TILE_SIZE, 0, 2 * Math.PI)
        ctx.fillStyle = "orange"
        ctx.fill()
        ctx.restore()
    }

    onRemoveNeighbour(nb: Structure){}

    renderAdditionalStructInfo(graphics: Graphics){
        const ctx = graphics.ctx
        let network = this.powerComponent!.network
        if(!network) return
        Graphics.renderBar(ctx, {x: 0, y: 0}, {w: 3 * Graphics.TILE_SIZE, h: Graphics.H_TILE_SIZE}, network.stored.val / network.totalCapacity, "green", 
            "Speicher (Netzwerk): " + Graphics.showNumber(network.stored.val) + " / " + Graphics.showNumber(network.totalCapacity) + " Wm", true)
        Graphics.renderInfoText(ctx, {x: 0, y: 0}, 0.5 * Graphics.TILE_SIZE, "Prod: " + network.productionCounter.query() + " Watt", true)
        Graphics.renderInfoText(ctx, {x: 0, y: 0}, 0.5 * Graphics.TILE_SIZE, "Cons: " + network.consumptionCounter.query() + " Watt", true)

    }
}

const IMG = loadImage("structures/power/node.png")