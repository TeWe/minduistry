import { World } from "../../world/world"
import { Averager } from "../util/averager"
import { CountDown } from "../util/countdown"
import { PerMinuteCounter } from "../util/per_minute_counter"
import { PowerComponent, PowerComponentType } from "./power_component"

export class PowerNetwork{
    components: Record<PowerComponentType, PowerComponent[]>
    totalCapacity: number
    productionCounter: PerMinuteCounter
    consumptionCounter: PerMinuteCounter

    efficiency: Averager
    stored: Averager

    ef: number
    totalStorageUpdater: CountDown

    constructor(){
        this.components = {
            "SINK": [],
            "SOURCE": [],
            "TRANSFER": [],
        }
        this.totalCapacity = 0
        this.productionCounter = new PerMinuteCounter()
        this.consumptionCounter = new PerMinuteCounter()
        this.efficiency = new Averager(5, 1.0)
        this.stored = new Averager(5, 0)
        this.ef = 1
        this.totalStorageUpdater = CountDown.perMinute(60, () => {this.stored.record(this.totalStored); return true})
    }

    private get sinks(): PowerComponent[]{
        return this.components[PowerComponentType.SINK]
    }

    private get sources(): PowerComponent[]{
        return this.components[PowerComponentType.SOURCE]
    }

    private get transfers(): PowerComponent[]{
        return this.components[PowerComponentType.TRANSFER]
    }
    
    addComponent(cmp: PowerComponent): void{
        cmp.network = this
        if(cmp.type == PowerComponentType.SINK) this.sinks.push(cmp)
        else if(cmp.type == PowerComponentType.SOURCE) this.sources.push(cmp)
        else if(cmp.type == PowerComponentType.TRANSFER){
            this.transfers.push(cmp)
            this.totalCapacity += cmp.storageCapacity
        }
        else throw "oh no! " + cmp.type
    }

    static recomputeNetworks(world: World): void{
        world.networks = []

        // reset existing networks
        world.networks = []
        world.forAllStructures(str => {
            if(str.powerComponent) str.powerComponent.network = undefined
        })

        // compute new ones
        world.forAllStructures(str => {
            if(!str.powerComponent || str.powerComponent.network) return
            let network = new PowerNetwork()
            PowerNetwork.growNetwork(network, str.powerComponent)
            world.networks.push(network)
        })
    }

    get totalStored(){
        return this.transfers.reduce((accu, cmp) => accu + cmp.stored, 0)
    }

    step(){
        this.distributePower()
        this.productionCounter.step()
        this.consumptionCounter.step()
        this.totalStorageUpdater.step()
    }

    grantPower(){
        return Math.random() > this.ef
    }

    distributePower(){
        let totalSupply = this.sources.reduce((accu, cmp) => accu + cmp.produced, 0)
        this.sources.forEach(source => source.produced = 0)
        this.productionCounter.register(totalSupply)

        let totalDemand = this.sinks.reduce((accu, cmp) => accu + cmp.demand, 0)
        this.sinks.forEach(sink => sink.demand = 0)
        this.consumptionCounter.register(totalDemand)

        let efficiency = 1.0

        if(totalSupply >= totalDemand){
            totalSupply -= totalDemand
            efficiency = 1.0

            let secondaryDemand = this.transfers.reduce((accu, cmp) => accu + cmp.availableStorageCapacity, 0)

            if(secondaryDemand > 0){
                let f = Math.min(1, totalSupply / secondaryDemand)
                this.transfers.forEach(cmp => cmp.fillStorageBy(f * cmp.availableStorageCapacity))
            }
        } else{
            // totalSupply < totalDemand && 0 < totalDemand
            let totalDemandTmp = totalDemand
            totalDemand -= totalSupply
            
            // use stored energy to fill the gap
            let secondarySupply = this.transfers.reduce((accu, cmp) => accu + cmp.stored, 0)
                
            let usedUp = Math.min(secondarySupply, totalDemand)
            let f = 0
            if(secondarySupply > 0) f = usedUp / secondarySupply
            this.transfers.forEach(cmp => cmp.drainStorageBy(f * cmp.stored))
            
            efficiency = usedUp / totalDemandTmp
        }

        this.sinks.forEach(sink => sink.providesPower = Math.random() <= efficiency)
        this.efficiency.record(efficiency)
        this.stored.record(this.totalStored)

    
    }

    static growNetwork(network: PowerNetwork, cmp: PowerComponent){
        network.addComponent(cmp)
        cmp.network = network
        for(let comp of cmp.adj){
            if(comp.network) continue
            PowerNetwork.growNetwork(network, comp)
        }
    }
}