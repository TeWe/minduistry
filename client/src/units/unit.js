class Unit{
    constructor(pos, world){
        this.pos = new Vector(pos.x, pos.y)
        this.world = world
        this.spawnCountDown = new CountDown(this.spawnSteps)
        this.goal = {x: 0, y: 0}
        this.velo = new Vector(0, 0)
        this.speed = 0.1
        this.renderRadius = this.radius * Graphics.TILE_SIZE
        this.hp = this.maxHP
    }

    getRandomDrift(){
        return {x: getRandomFloatingPoint(-this.randomness, this.randomness), 
                y: getRandomFloatingPoint(-this.randomness, this.randomness)}
    }

    onDeath(){}

    get team(){
        return true
    }

    get radius(){
        return 0.5
    }
    
    get maxHP(){
        return 20
    }

    // This might need to be updated in the future for performace
    move(){
        this.pos.x += this.velo.x
        this.pos.y += this.velo.y

        let struct = this.world.structureAtPos(this.pos)
        if(struct && struct.team != this.team && struct.isSolid()){
            this.pos.x -= this.velo.x
            this.pos.y -= this.velo.y 
            this.velo.x = 0
            this.velo.y = 0
        }

        if(struct && !(struct instanceof Empty) && struct.team != this.team){
            struct.takeDamage(this.atk)
            this.takeDamage(struct.atk)
            // knock unit back to make attack look better
            this.velo = struct.midPos.to(this.pos).scaledTo(1)
        }


        if(this.pos.x < 0) {
            this.pos.x = 0
            this.velo.x = 0
        }
        if(this.pos.x > this.world.w + 1) {
            this.pos.x = this.world.w + 1
            this.velo.x = 0
        }
        if(this.pos.y < 0){
            this.pos.y = 0
            this.velo.y = 0
        } 
        if(this.pos.y > this.world.h + 1){
            this.pos.y = this.world.h + 1
            this.velo.y = 0
        } 

    }

    receiveAttackCommand(str){
        this.attackGoal = str
    }

    getAIDrift(){
        if(this.attackGoal) return this.pos.to(this.attackGoal.midPos).scaledTo(0.05) 
        else return this.pos.to(this.homeStructure.midPos).scaledBy(0.004)
    }

    get atk(){
        return 1
    }

    get spawnSteps(){
        return 50
    }
        
    onAttackGoalDestroyed(){
        this.attackGoal = undefined
    }

    get spawnProgres(){
        return this.stepsSinceSpawn / this.spawnSteps
    }


    takeDamage(dmg){
        this.hp -= dmg
        if(this.hp <= 0) this.die()
    }

    die(){
        this.world.units = this.world.units.filter(unit => unit != this)
        this.onDeath()
    }

    get spawning(){
        return !this.spawnCountDown.completed
    }

    step(){
        if(this.spawning){
            this.spawnCountDown.step()
            return
        } 

        this.velo.add(this.getRandomDrift())
        this.velo.add(this.getAIDrift())
        this.velo.scaleTo(this.speed)
        this.move()
    }

    render(ctx){
        ctx.beginPath()
        ctx.arc(0, 0, Graphics.H_TILE_SIZE, 0, 2 * Math.PI)
        ctx.strokeStyle = "red"
        ctx.stroke()
    }
}