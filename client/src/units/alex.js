class Alex extends Unit{
    constructor(pos, world){
        super(pos, world)

        this.goal = {x: 0, y: 0}
        this.attackGoal = undefined
    }

    get radius(){
        return 0.5
    }

    get atk(){
        return 4
    }

    get maxHP(){
        return 
    }

    onAttackGoalDestroyed(){
        this.attackGoal = this.world.getClosestStructure(this.pos, str => str != this.attackGoal && (!(str instanceof Empty)) && str.team != this.team)
    }

    onDeath(){
        if(this.homeStructure){
            this.homeStructure.units = this.homeStructure.units.filter(unit => unit != this)
        }
    }

    get randomness(){
        return 0.05
    }

    get team(){
        return false
    }

    render(ctx){
        ctx.save()
        ctx.globalAlpha = this.stepsSinceSpawn / this.spawnSteps * this.stepsSinceSpawn / this.spawnSteps
        ctx.drawImage(Alex.img, -this.renderRadius, -this.renderRadius, 2 * this.renderRadius, 2 * this.renderRadius)
        ctx.restore()
    }
}

Alex.img = loadImage("units/alex.png")