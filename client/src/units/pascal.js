class Pascal extends Unit{
    constructor(pos, world){
        super(pos, world)

        let str = world.getClosestStructure(pos, str => str.team != this.team)
        this.receiveAttackCommand(str)
    }

    get radius(){
        return 0.5
    }

    get atk(){
        return 5
    }

    get maxHP(){
        return 18
    }

    get team(){
        return true
    }


    onDeath(){
        
    }

    get randomness(){
        return 0.03
    }

    get team(){
        return true
    }

    render(ctx){
        ctx.save()
        ctx.globalAlpha = this.stepsSinceSpawn / this.spawnSteps * this.stepsSinceSpawn / this.spawnSteps
        ctx.drawImage(Pascal.img, -this.renderRadius, -this.renderRadius, 2 * this.renderRadius, 2 * this.renderRadius)
        ctx.restore()
    }
}

Pascal.img = loadImage("units/pascal.png")