export type ScreenPos = Readonly<{x: number, y: number}>
export type TilePos = Readonly<{j: number, i: number}>
export type Dimensions = Readonly<{w: number, h: number}>

export function interpolate(pos1: ScreenPos, pos2: ScreenPos, fraction: number): ScreenPos {
    return { x: pos1.x + fraction * (pos2.x - pos1.x), y: pos1.y + fraction * (pos2.y - pos1.y) }
}

export type Shape = Rectangle

export function roundTilePos(pos: TilePos): TilePos{
    return {j: Math.floor(pos.j), i: Math.floor(pos.i)}
}

export function tilePosEq(pos1: TilePos, pos2: TilePos): boolean{
    return pos1.i == pos2.i && pos1.j == pos2.j
}

export function screenPos(x: number, y: number): ScreenPos{
    return {x: x, y: y}
}

export function tilePos(j: number, i: number): TilePos{
    return {j: j, i: i}
}

export function tileDistance(p1: TilePos, p2: TilePos): number{
    return Math.sqrt((p1.j - p2.j) * (p1.j - p2.j) + (p1.i - p2.i) * (p1.i - p2.i))
}

export class Rectangle{
    x0: number
    y0: number
    w: number
    h: number
    constructor(x0: number, y0: number, w: number, h: number){
        this.x0 = x0,
        this.y0 = y0,
        this.w = w,
        this.h = h
    }

    get anc(): ScreenPos{
        return {x: this.x0, y: this.y0}
    }
    get bounds(): Dimensions{
        return {w: this.w, h: this.h}
    }

    contains(p: ScreenPos): boolean{
        return this.x0 <= p.x && p.x <= this.x0 + this.w && this.y0 <= p.y && p.y <= this.y0 + this.h 
    }
    
    ctxRect(ctx: CanvasRenderingContext2D): void{
        ctx.rect(this.x0, this.y0, this.w, this.h)
    }
}