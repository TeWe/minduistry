
export const enum Direction{
    UP = "UP",
    DOWN = "DOWN",
    LEFT = "LEFT",
    RIGHT = "RIGHT"
}

export function opposite(dir: Direction): Direction{
    switch (dir) {
        case Direction.UP: return Direction.DOWN
        case Direction.DOWN: return Direction.UP
        case Direction.LEFT: return Direction.RIGHT
        case Direction.RIGHT: return Direction.LEFT
    }
}

export function clockwise(dir: Direction): Direction{
    switch (dir) {
        case Direction.UP: return Direction.LEFT
        case Direction.DOWN: return Direction.RIGHT
        case Direction.LEFT: return Direction.DOWN
        case Direction.RIGHT: return Direction.UP
    }
}

export function counterClockwise(dir: Direction): Direction{
    switch (dir) {
        case Direction.UP: return Direction.RIGHT
        case Direction.DOWN: return Direction.LEFT
        case Direction.LEFT: return Direction.UP
        case Direction.RIGHT: return Direction.DOWN
    }
}

export function forAllDirections(f: (dir: Direction) => void): void{
    f(Direction.DOWN)
    f(Direction.UP)
    f(Direction.LEFT)
    f(Direction.RIGHT)
}